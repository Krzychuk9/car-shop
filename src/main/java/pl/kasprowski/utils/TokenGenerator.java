package pl.kasprowski.utils;

import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
public class TokenGenerator {

    public String getToken(String s) {
        MessageDigest mdEncryptor = null;
        String token = "";
        try {
            mdEncryptor = MessageDigest.getInstance("MD5");
            token = new HexBinaryAdapter().marshal(mdEncryptor.digest(s.getBytes())) + Math.random() * 1000000;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return token;
    }
}
