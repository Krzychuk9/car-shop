package pl.kasprowski.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.kasprowski.dto.NewCarDto;
import pl.kasprowski.entity.Car;
import pl.kasprowski.entity.Fuel;
import pl.kasprowski.entity.Gearbox;
import pl.kasprowski.repositories.CarRepository;
import pl.kasprowski.repositories.FuelRepository;
import pl.kasprowski.repositories.GearboxRepository;

@Component
public class NewCarDtoConverter implements Converter<NewCarDto, Car> {

    @Autowired
    private CarRepository carRepository;
    @Autowired
    private FuelRepository fuelRepository;
    @Autowired
    private GearboxRepository gearboxRepository;


    @Override
    public Car convert(NewCarDto dto) {
        Car car = new Car();
        Fuel fuel = fuelRepository.findOne(dto.getFuelType());
        Gearbox gearbox = gearboxRepository.findOne(dto.getGearbox());

        car.setDescription(dto.getDescription());
        car.setEngineCapacity(dto.getEngineCapacity());
        car.setLiabilityNumber(dto.getLiabilityNumber());
        car.setMark(dto.getMark());
        car.setMileage(dto.getMileage());
        car.setModel(dto.getModel());
        car.setPower(dto.getPower());
        car.setRegistrationNumber(dto.getRegistrationNumber());
        car.setYear(dto.getYear());
        car.setVin(dto.getVin());
        car.setSold(false);
        car.setFuelType(fuel);
        car.setGearbox(gearbox);

        if (dto.getId() != null) {
            car.setId(dto.getId());
            Car carFromDb = carRepository.getOne(dto.getId());
            car.setImageUrl(dto.getImageUrl());
            car.setTestDrives(carFromDb.getTestDrives());
            car.setOwner(carFromDb.getOwner());
        } else {
            car.setTestDrives(0);
            car.setImageUrl("/img/cars/default.jpg");
        }

        return car;
    }
}
