package pl.kasprowski.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.kasprowski.dto.PersonDto;
import pl.kasprowski.entity.Address;
import pl.kasprowski.entity.Employee;
import pl.kasprowski.entity.Person;

@Component
public class PersonDtoConverter implements Converter<Person, PersonDto> {

    @Override
    public PersonDto convert(Person person) {
        PersonDto dto = new PersonDto();

        dto.setId(person.getId());
        dto.setName(person.getName());
        dto.setSurname(person.getSurname());
        dto.setNip(person.getNip());
        dto.setPesel(person.getPesel());

        Address address = person.getAddress();
        if(address != null){
            dto.setStreet(address.getStreet());
            dto.setCity(address.getCity());
            dto.setCountry(address.getCountry());
        }

        if (person.getUser() != null) {
            dto.setUsername(person.getUser().getUsername());
            dto.setEmail(person.getUser().getEmail());
        }

        if (person instanceof Employee) {
            Employee emp = (Employee) person;
            dto.setDateHired(emp.getHireDate().toString());
            dto.setEmployee(true);
        }

        return dto;
    }
}
