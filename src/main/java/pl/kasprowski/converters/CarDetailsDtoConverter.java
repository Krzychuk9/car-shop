package pl.kasprowski.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.kasprowski.dto.CarDetailsDto;
import pl.kasprowski.entity.Agreement;
import pl.kasprowski.entity.Car;

@Component
public class CarDetailsDtoConverter implements Converter<Agreement, CarDetailsDto> {

    @Override
    public CarDetailsDto convert(Agreement agreement) {
        CarDetailsDto dto = new CarDetailsDto();
        Car car = agreement.getCar();

        dto.setVin(car.getVin());
        dto.setYear(car.getYear().toString());
        dto.setMark(car.getMark());
        dto.setModel(car.getModel());
        dto.setLiabilityNumber(car.getLiabilityNumber());
        dto.setRegistrationNumber(car.getRegistrationNumber());
        dto.setFuelType(car.getFuelType().getName());
        dto.setMileage(car.getMileage().toString());
        dto.setEngineCapacity(car.getEngineCapacity().toString());
        dto.setPower(car.getPower().toString());
        dto.setGearbox(car.getGearbox().getName());
        dto.setDescription(car.getDescription());
        dto.setTestDrives(car.getTestDrives().toString());
        dto.setImageUrl(car.getImageUrl());
        dto.setName(car.getOwner().getName());
        dto.setSurname(car.getOwner().getSurname());
        dto.setPrice(agreement.getAmount());
        dto.setAgreementId(agreement.getAgreementType().getId());

        return dto;
    }
}
