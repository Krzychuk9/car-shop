package pl.kasprowski.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.kasprowski.dto.ReportDto;
import pl.kasprowski.entity.ReportData;
import pl.kasprowski.exceptions.ReportsException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ReportDtoConverter implements Converter<ReportDto, ReportData> {

    @Override
    public ReportData convert(ReportDto dto) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        ReportData reportData = new ReportData();
        reportData.setTypId(dto.getReportType());
        Date dateTo;
        Date dateFrom;

        try {
            dateTo = formatter.parse(dto.getDateTo());
            reportData.setDateTo(dateTo);
            dateFrom = formatter.parse(dto.getDateFrom());
            reportData.setDateFrom(dateFrom);
        } catch (ParseException e) {
            throw new ReportsException("Zły format daty!");
        }

        if (dateTo.getTime() - dateFrom.getTime() < 0)
            throw new ReportsException("Podano nieprawidłowy okres czasu!");

        return reportData;
    }
}
