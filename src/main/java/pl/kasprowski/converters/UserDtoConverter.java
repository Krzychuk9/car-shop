package pl.kasprowski.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.kasprowski.dto.UserDto;
import pl.kasprowski.entity.User;
import pl.kasprowski.repositories.RoleRepository;
import pl.kasprowski.utils.TokenGenerator;

import java.util.HashSet;

@Component
public class UserDtoConverter implements Converter<UserDto, User> {

    @Autowired
    private BCryptPasswordEncoder encoder;
    @Autowired
    private TokenGenerator generator;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public User convert(UserDto dto) {
        User user = new User();

        user.setUsername(dto.getUsername());
        user.setPassword(encoder.encode(dto.getPassword()));
        user.setEmail(dto.getEmail());
        user.setEnabled(false);
        user.setAccountNotExpired(true);
        user.setCredentialsNotExpired(true);
        user.setAccountNotLocked(true);
        user.setRoles(new HashSet<>());
        user.getRoles().add(roleRepository.findOne(2));
        user.setToken(generator.getToken(dto.getUsername()));

        return user;
    }
}
