package pl.kasprowski.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.kasprowski.dto.NotApprovedAgreementDto;
import pl.kasprowski.entity.Agreement;

@Component
public class NotApprovedAgreementDtoConverter implements Converter<Agreement, NotApprovedAgreementDto> {

    @Override
    public NotApprovedAgreementDto convert(Agreement agreement) {
        NotApprovedAgreementDto dto = new NotApprovedAgreementDto();

        dto.setId(agreement.getId());
        dto.setContent(agreement.getContent());
        dto.setName(agreement.getPerson().getName());
        dto.setSurname(agreement.getPerson().getSurname());

        return dto;
    }
}
