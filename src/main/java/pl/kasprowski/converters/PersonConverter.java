package pl.kasprowski.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.kasprowski.dto.PersonDto;
import pl.kasprowski.entity.Address;
import pl.kasprowski.entity.Customer;
import pl.kasprowski.entity.Employee;
import pl.kasprowski.entity.Person;
import pl.kasprowski.repositories.EmployeeRepository;
import pl.kasprowski.repositories.UserRepository;

import java.util.Date;

@Component
public class PersonConverter implements Converter<PersonDto, Person> {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public Person convert(PersonDto personDto) {
        Person person = null;

        if (personDto.isEmployee()) {
            person = new Employee();
            Employee emp = (Employee) person;
            if (personDto.getDateHired().equals("")) {
                emp.setHireDate(new Date());
            } else {
                emp.setHireDate(employeeRepository.findOne(personDto.getId()).getHireDate());
            }
        } else {
            person = new Customer();
        }

        Address address = new Address();

        person.setId(personDto.getId());
        person.setName(personDto.getName());
        person.setSurname(personDto.getSurname());
        person.setNip(personDto.getNip());
        person.setPesel(personDto.getPesel());

        address.setCity(personDto.getCity());
        address.setCountry(personDto.getCountry());
        address.setStreet(personDto.getStreet());
        person.setAddress(address);

        if (!personDto.getUsername().isEmpty()) {
            person.setUser(userRepository.findByUsername(personDto.getUsername()));
        }

        return person;
    }
}
