package pl.kasprowski.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.kasprowski.dto.CarDto;
import pl.kasprowski.entity.Agreement;
import pl.kasprowski.entity.Car;

@Component
public class AgreementToOfferDtoConverter implements Converter<Agreement, CarDto> {


    @Override
    public CarDto convert(Agreement agreement) {
        CarDto dto = new CarDto();
        Car car = agreement.getCar();

        dto.setId(car.getId());
        dto.setMileage(car.getMileage());
        dto.setEngineCapacity(car.getEngineCapacity());
        dto.setFuelType(car.getFuelType().getName());
        dto.setMark(car.getMark());
        dto.setModel(car.getModel());
        dto.setYear(car.getYear());
        dto.setPower(car.getPower());
        dto.setImageUrl(car.getImageUrl());
        dto.setPrice(agreement.getAmount());

        return dto;
    }
}