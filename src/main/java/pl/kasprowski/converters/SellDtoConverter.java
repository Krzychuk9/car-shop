package pl.kasprowski.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.kasprowski.dto.SellDto;
import pl.kasprowski.entity.Address;
import pl.kasprowski.entity.Customer;
import pl.kasprowski.entity.Person;

@Component
public class SellDtoConverter implements Converter<SellDto, Person> {


    @Override
    public Person convert(SellDto sellDto) {
        Person person = new Customer();
        Address address = new Address();

        person.setName(sellDto.getName());
        person.setSurname(sellDto.getSurname());
        person.setNip(sellDto.getNip());
        person.setPesel(sellDto.getPesel());

        address.setCity(sellDto.getCity());
        address.setCountry(sellDto.getCountry());
        address.setStreet(sellDto.getStreet());
        person.setAddress(address);

        return person;
    }
}
