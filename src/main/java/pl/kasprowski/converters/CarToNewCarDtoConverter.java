package pl.kasprowski.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.kasprowski.dto.FuelDto;
import pl.kasprowski.dto.GearBoxDto;
import pl.kasprowski.dto.NewCarDto;
import pl.kasprowski.entity.Car;
import pl.kasprowski.entity.Fuel;
import pl.kasprowski.entity.Gearbox;

@Component
public class CarToNewCarDtoConverter implements Converter<Car, NewCarDto> {

    @Override
    public NewCarDto convert(Car car) {
        NewCarDto dto = new NewCarDto();

        dto.setId(car.getId());
        dto.setImageUrl(car.getImageUrl());
        dto.setDescription(car.getDescription());
        dto.setEngineCapacity(car.getEngineCapacity());
        dto.setLiabilityNumber(car.getLiabilityNumber());
        dto.setMark(car.getMark());
        dto.setMileage(car.getMileage());
        dto.setModel(car.getModel());
        dto.setPower(car.getPower());
        dto.setRegistrationNumber(car.getRegistrationNumber());
        dto.setYear(car.getYear());
        dto.setVin(car.getVin());

        Fuel fuelType = car.getFuelType();
        dto.setFuelType(fuelType.getId());

        Gearbox gearbox = car.getGearbox();
        dto.setGearbox(gearbox.getId());

        return dto;
    }
}
