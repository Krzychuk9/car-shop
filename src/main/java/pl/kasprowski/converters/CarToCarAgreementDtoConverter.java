package pl.kasprowski.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.kasprowski.dto.CarAgreementDto;
import pl.kasprowski.entity.Agreement;
import pl.kasprowski.entity.Car;
import pl.kasprowski.entity.Fuel;
import pl.kasprowski.entity.Gearbox;
import pl.kasprowski.services.AgreementService;

@Component
public class CarToCarAgreementDtoConverter implements Converter<Car, CarAgreementDto> {

    @Autowired
    private AgreementService agreementService;

    @Override
    public CarAgreementDto convert(Car car) {
        Agreement agreement = agreementService.findAgreementByCarId(car.getId());
        CarAgreementDto dto = new CarAgreementDto();

        dto.setId(car.getId());
        dto.setImageUrl(car.getImageUrl());
        dto.setDescription(car.getDescription());
        dto.setEngineCapacity(car.getEngineCapacity());
        dto.setLiabilityNumber(car.getLiabilityNumber());
        dto.setMark(car.getMark());
        dto.setMileage(car.getMileage());
        dto.setModel(car.getModel());
        dto.setPower(car.getPower());
        dto.setRegistrationNumber(car.getRegistrationNumber());
        dto.setYear(car.getYear());
        dto.setVin(car.getVin());

        Fuel fuelType = car.getFuelType();
        dto.setFuelType(fuelType.getId());

        Gearbox gearbox = car.getGearbox();
        dto.setGearbox(gearbox.getId());

        dto.setContent(agreement.getContent());
        dto.setAmount(agreement.getAmount());
        dto.setAgreementTypeId(agreement.getAgreementType().getId());

        dto.setName(car.getOwner().getName());
        dto.setSurname(car.getOwner().getSurname());
        dto.setPesel(car.getOwner().getPesel());
        
        return dto;
    }
}
