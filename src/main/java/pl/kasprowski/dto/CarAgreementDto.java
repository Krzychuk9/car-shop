package pl.kasprowski.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class CarAgreementDto {

    private Integer id;
    @NotEmpty
    private String mark;
    @NotEmpty
    private String model;
    @NotNull
    @Digits(integer = 4, fraction = 0)
    private Integer year;
    @NotNull
    @Digits(integer = 6, fraction = 0)
    private Integer mileage;
    @NotEmpty
    private String vin;
    @NotNull
    private Integer fuelType;
    @NotEmpty
    private String description;
    @NotNull
    @Digits(integer = 4, fraction = 0)
    private Integer engineCapacity;
    @NotNull
    @Digits(integer = 3, fraction = 0)
    private Integer power;
    @NotNull
    private Integer gearbox;
    @NotEmpty
    private String liabilityNumber;
    @NotEmpty
    private String registrationNumber;
    private String imageUrl;
    private Integer agreementTypeId;
    private String name;
    private String surname;
    private String pesel;
    @NotEmpty
    private String content;
    @DecimalMin(value = "5000")
    @NotNull
    private BigDecimal amount;

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getFuelType() {
        return fuelType;
    }

    public void setFuelType(Integer fuelType) {
        this.fuelType = fuelType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(Integer engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Integer getGearbox() {
        return gearbox;
    }

    public void setGearbox(Integer gearbox) {
        this.gearbox = gearbox;
    }

    public String getLiabilityNumber() {
        return liabilityNumber;
    }

    public void setLiabilityNumber(String liabilityNumber) {
        this.liabilityNumber = liabilityNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getAgreementTypeId() {
        return agreementTypeId;
    }

    public void setAgreementTypeId(Integer agreementTypeId) {
        this.agreementTypeId = agreementTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}

