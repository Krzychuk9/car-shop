package pl.kasprowski.dto;

import pl.kasprowski.entity.Report;

import java.math.BigDecimal;
import java.util.List;

public class GeneratedReportDto {

    private String dateTo;
    private String dateFrom;
    private List<Report> agreements;
    private Integer type;
    private BigDecimal sum;
    private BigDecimal profit;

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public List<Report> getAgreements() {
        return agreements;
    }

    public void setAgreements(List<Report> agreements) {
        this.agreements = agreements;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }
}
