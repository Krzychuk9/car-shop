package pl.kasprowski.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class UserDetailsDto {

    @NotNull
    private Integer id;
    private boolean accountActive;
    private boolean notBlocked;
    @NotEmpty
    private String username;
    @NotEmpty
    private String email;
    @NotNull
    private Integer roleId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isAccountActive() {
        return accountActive;
    }

    public void setAccountActive(boolean accountActive) {
        this.accountActive = accountActive;
    }

    public boolean isNotBlocked() {
        return notBlocked;
    }

    public void setNotBlocked(boolean notBlocked) {
        this.notBlocked = notBlocked;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}
