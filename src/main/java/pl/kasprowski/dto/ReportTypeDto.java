package pl.kasprowski.dto;

public class ReportTypeDto {

    private Integer id;
    private String name;

    public ReportTypeDto() {
    }

    public ReportTypeDto(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
