package pl.kasprowski.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private BCryptPasswordEncoder encoder;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/css/**", "/img/**").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/car/list").permitAll()
                .antMatchers("/registration","/confirmation","/password","/resetpassword","/","/contact").permitAll()
                .antMatchers("/car/details","/car/update/*","/account","/car/search").hasAnyAuthority("ROLE_USER","ROLE_EMPLOYEE","ROLE_TRADER","ROLE_MANAGER","ROLE_ADMIN")
                .antMatchers("/car/add","/car/testdrive/*","/account/myCars","/account/myCars/details").hasAnyAuthority("ROLE_USER","ROLE_ADMIN")
                .antMatchers("/car/management/sold","/car/management/delete/*","/car/management/add/purchase","/car/management/add/cession","/car/management/update/*","/car/management/list/purchase","/car/management/list/cession").hasAnyAuthority("ROLE_EMPLOYEE","ROLE_TRADER","ROLE_MANAGER","ROLE_ADMIN")
                .antMatchers("/car/management/offers","/car/management/offers/details","/car/management/sell/*").hasAnyAuthority("ROLE_TRADER","ROLE_ADMIN","ROLE_MANAGER")
                .antMatchers("/car/management/buy/*").hasAuthority("ROLE_MANAGER")
                .antMatchers("/report","/user/add","/user/list/clients","/user/list/employee","/user/delete","/user/update","/user/details").hasAnyAuthority("ROLE_MANAGER","ROLE_ADMIN")
                .antMatchers("/admin/*").hasAuthority("ROLE_ADMIN")
                .anyRequest().authenticated()
                .and()
                .csrf().disable()
                .formLogin()
                .loginPage("/login").defaultSuccessUrl("/car/list",true).permitAll()
                .and()
                .logout().logoutSuccessUrl("/car/list").logoutUrl("/logout").permitAll();
    }


    @Override
    @Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(encoder);
        auth.inMemoryAuthentication().withUser("a@a").password("admin").roles("ADMIN");
    }
}
