package pl.kasprowski.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.kasprowski.dto.UserDto;
import pl.kasprowski.entity.EmailType;
import pl.kasprowski.services.EmailService;
import pl.kasprowski.services.UserService;
import pl.kasprowski.validators.UserValidator;

import javax.validation.Valid;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private UserValidator userValidator;

    @GetMapping("/login")
    public ModelAndView showLoginPage(ModelMap modelMap) {
        return new ModelAndView("login");
    }

    @GetMapping("/registration")
    public ModelAndView showRegistrationPage(ModelMap map) {
        map.addAttribute("userDto", new UserDto());
        return new ModelAndView("registration", map);
    }

    @PostMapping("/registration")
    public ModelAndView registerNewUser(@ModelAttribute @Valid UserDto userDto, BindingResult result, ModelMap map) {

        userValidator.validate(userDto, result);
        if (result.hasErrors()) {
            return new ModelAndView("registration");
        }
        userService.save(userDto);
        emailService.sendEmail(userDto, EmailType.ACCOUNT_CONFIRMATION);
        map.addAttribute("registrationMessage", "Rejestracja przebiegła pomyślnie, sprawdź swoją skrzynkę pocztową!");
        return new ModelAndView("login", map);
    }

    @GetMapping("/confirmation")
    public ModelAndView confirmUserAccount(@RequestParam("token") String token, ModelMap map) {

        userService.confirmAccount(token);

        map.addAttribute("registrationMessage", "Konto aktywowane!");
        return new ModelAndView("login", map);
    }

    @GetMapping("/resetpassword")
    public ModelAndView showResetPasswordPage(ModelMap map) {
        map.addAttribute("userDto", new UserDto());
        return new ModelAndView("account/resetPassword", map);
    }

    @PostMapping("/resetpassword")
    public ModelAndView resetPassword(@ModelAttribute UserDto userDto, ModelMap map) {

        userService.resetPassword(userDto);
        emailService.sendEmail(userDto, EmailType.PASSWORD_RESET);

        map.addAttribute("registrationMessage", "Sprawdź swoją skrzynkę pocztową!");
        return new ModelAndView("login", map);
    }

    @GetMapping("/password")
    public ModelAndView showNewPasswordPage(@RequestParam("token") String token, ModelMap map) {
        map.addAttribute("userDto", userService.getUserDtoByToken(token));
        return new ModelAndView("account/newPassword", map);
    }

    @PostMapping("/password")
    public ModelAndView setNewPassword(@ModelAttribute @Valid UserDto userDto, BindingResult result, ModelMap map) {

        userValidator.validatePasswordAndRejectErrors(userDto, result);
        if (result.hasErrors()) {
            return new ModelAndView("account/newPassword");
        }

        userService.setNewPassword(userDto);
        map.addAttribute("registrationMessage", "Hasło użytkownika: " + userDto.getUsername() + " zmienione!");
        return new ModelAndView("login", map);
    }
}
