package pl.kasprowski.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Controller
@RequestMapping("/img")
public class ImageController {

    @Value("${img.file.path}")
    private String PATH;

    @RequestMapping(value = "/cars/{imageName}")
    @ResponseBody
    public byte[] getImage(@PathVariable("imageName") String imageName) throws IOException {

        File serverFile = new File(PATH + imageName + ".jpg");

        return Files.readAllBytes(serverFile.toPath());
    }
}
