package pl.kasprowski.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.kasprowski.dto.PersonDto;
import pl.kasprowski.entity.User;
import pl.kasprowski.services.CarService;
import pl.kasprowski.services.PersonService;
import pl.kasprowski.services.SecurityService;
import pl.kasprowski.services.UserService;
import pl.kasprowski.validators.PersonValidator;

import javax.validation.Valid;

@Controller
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private UserService userService;
    @Autowired
    private PersonService personService;
    @Autowired
    private CarService carService;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private PersonValidator personValidator;


    @GetMapping("/details")
    public ModelAndView showAccountDetails(ModelMap map) {
        User loggedInUser = securityService.findLoggedInUser();
        map.addAttribute("personDto", userService.findPersonDtoByUser(loggedInUser));
        return new ModelAndView("person/personDetails", map);
    }

    @GetMapping("/addDetails")
    public ModelAndView showAddAccountDetailsPage(ModelMap map) {
        User loggedInUser = securityService.findLoggedInUser();
        PersonDto dto = new PersonDto();
        dto.setUsername(loggedInUser.getUsername());
        dto.setEmail(loggedInUser.getEmail());
        map.addAttribute("personDto", dto);
        return new ModelAndView("account/newForm", map);
    }

    @GetMapping("/update")
    public ModelAndView showUpdateAccountDetailsPage(ModelMap map) {
        User loggedInUser = securityService.findLoggedInUser();
        map.addAttribute("personDto", userService.findPersonDtoByUser(loggedInUser));
        return new ModelAndView("account/newForm", map);
    }

    @PostMapping("/addDetails")
    public ModelAndView saveOrUpdateUser(@ModelAttribute @Valid PersonDto personDto, BindingResult result, ModelMap map) {

        personValidator.validate(personDto, result);
        if (result.hasErrors()) {
            return new ModelAndView("account/newForm");
        }
        personService.saveOrUpdateUser(personDto);
        map.addAttribute("personDto", personDto);
        return new ModelAndView("person/personDetails", map);
    }


    @GetMapping("/myCars")
    public ModelAndView showMyCarsList(ModelMap map) {
        map.addAttribute("cars", carService.getAllCarsByLoggedUser());
        map.addAttribute("title", "Moje samochody");
        map.addAttribute("isOwner", true);
        return new ModelAndView("car/carlist", map);
    }

    @GetMapping("/myCars/details")
    public ModelAndView showCarDetailsPage(@RequestParam("id") Integer id, ModelMap map) {
        map.addAttribute("carDto", carService.getCarDetailsById(id));
        map.addAttribute("id", id);
        return new ModelAndView("car/mydetails", map);
    }
}
