package pl.kasprowski.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.kasprowski.dto.NewCarDto;
import pl.kasprowski.entity.EmailType;
import pl.kasprowski.entity.User;
import pl.kasprowski.services.CarService;
import pl.kasprowski.services.EmailService;
import pl.kasprowski.services.SecurityService;
import pl.kasprowski.validators.CarValidator;

import javax.validation.Valid;

@Controller
@RequestMapping("/car")
public class CarController {

    @Autowired
    private CarService carService;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private CarValidator carValidator;

    @GetMapping("/list")
    public ModelAndView showCarToSellList(ModelMap map) {
        map.addAttribute("cars", carService.getAllCarsToSell());
        return new ModelAndView("car/carlist", map);
    }

    @GetMapping("/details")
    public ModelAndView showCarDetailsPage(@RequestParam("id") Integer id, ModelMap map) {
        map.addAttribute("carDto", carService.getCarDetailsById(id));
        map.addAttribute("id", id);
        return new ModelAndView("car/details", map);
    }

    @GetMapping("/add")
    public ModelAndView showAddNewCarPage(ModelMap map) {
        securityService.isAccountFullyActivated();
        map.addAttribute("newCarDto", new NewCarDto());
        map.addAttribute("fuelTypes", carService.getAllFuelTypes());
        map.addAttribute("gearboxes", carService.getAllGearboxes());
        return new ModelAndView("car/carNewForm", map);
    }

    @GetMapping("/update/{id}")
    public ModelAndView showUpdateCarPage(@PathVariable Integer id, ModelMap map) {
        map.addAttribute("newCarDto", carService.getCarForUpdateByUser(id));
        map.addAttribute("fuelTypes", carService.getAllFuelTypes());
        map.addAttribute("gearboxes", carService.getAllGearboxes());
        return new ModelAndView("car/carNewForm", map);
    }

    @PostMapping("/add")
    public ModelAndView addNewCar(@ModelAttribute @Valid NewCarDto newCarDto, BindingResult result, @RequestPart("file") MultipartFile file, ModelMap map) {

        carValidator.validate(newCarDto, result);
        if (result.hasErrors()) {
            map.addAttribute("fuelTypes", carService.getAllFuelTypes());
            map.addAttribute("gearboxes", carService.getAllGearboxes());
            return new ModelAndView("car/carNewForm",map);
        }

        if (newCarDto.getId() == null) {
            carService.registerNewCarWithAgreementByUser(newCarDto, file);
        } else {
            carService.updateExistingCarDetails(newCarDto, file);
        }

        return new ModelAndView("redirect:/car/list");
    }

    @GetMapping("/testdrive/{id}")
    public String orderTestDrive(@PathVariable Integer id, RedirectAttributes redirectAttributes) {
        User loggedInUser = securityService.findLoggedInUser();
        String vin = carService.getVinByCarId(id);

        emailService.sendNotificationEmail(loggedInUser, vin, EmailType.NOTIFICATION);
        carService.increaseTestDriveCount(id);

        redirectAttributes.addFlashAttribute("testDrive", "Wkrótce nasz przedstawiciel skontaktuje się z Państwem!");
        return "redirect:/car/details?id=" + id;
    }

}
