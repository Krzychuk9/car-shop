package pl.kasprowski.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.kasprowski.services.SearchService;

@Controller
@RequestMapping("/car/search")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @PostMapping
    public ModelAndView search(@RequestParam(value = "searchDto") String searchDto, ModelMap map) {
        map.addAttribute("cars", searchService.getAllCarsToSell(searchDto));
        return new ModelAndView("car/carlist", map);
    }
}
