package pl.kasprowski.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.kasprowski.exceptions.AccountNotFullyActivatedException;
import pl.kasprowski.exceptions.ReportsException;
import pl.kasprowski.exceptions.UserNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@ControllerAdvice
public class ExceptionController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(HttpServletRequest request, Exception ex) {
        logger.error(ex.getMessage());
        logger.error(request.getRequestURI());
        Arrays.stream(ex.getStackTrace()).forEach(s -> logger.debug(s.toString()));

        ModelAndView mav = new ModelAndView();
        mav.setViewName("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ModelAndView handleUserNotFoundException(UserNotFoundException ex) {
        logger.debug(ex.getMessage());

        ModelAndView mav = new ModelAndView();
        mav.setViewName("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ExceptionHandler(AccountNotFullyActivatedException.class)
    public ModelAndView handleAccountNotFullyActivatedException(AccountNotFullyActivatedException ex) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("redirect:/account/addDetails");
        return mav;
    }

    @ExceptionHandler(ReportsException.class)
    public ModelAndView handleReportsException(ReportsException ex, RedirectAttributes redirectAttributes) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("redirect:/report");
        redirectAttributes.addFlashAttribute("errorMessage", ex.getMessage());
        return mav;
    }
}
