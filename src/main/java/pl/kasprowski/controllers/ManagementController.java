package pl.kasprowski.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import pl.kasprowski.dto.CarAgreementDto;
import pl.kasprowski.dto.NotApprovedAgreementDto;
import pl.kasprowski.dto.SellDto;
import pl.kasprowski.services.AgreementService;
import pl.kasprowski.services.CarService;
import pl.kasprowski.services.PersonService;
import pl.kasprowski.validators.CarValidator;
import pl.kasprowski.validators.PersonValidator;

import javax.validation.Valid;

@Controller
@RequestMapping("/car/management")
public class ManagementController {

    @Autowired
    private CarService carService;
    @Autowired
    private AgreementService agreementService;
    @Autowired
    private CarValidator carValidator;
    @Autowired
    private PersonValidator personValidator;
    @Autowired
    private PersonService personService;

    @GetMapping("/sold")
    public ModelAndView showSoldCarsList(ModelMap map) {
        map.addAttribute("cars", carService.getAllSoldCars());
        return new ModelAndView("car/soldCarsList", map);
    }

    @GetMapping("/offers")
    public ModelAndView showOffersToAcceptation(ModelMap map) {
        map.addAttribute("offers", agreementService.getOffersToConfirm());
        return new ModelAndView("agreements/offers", map);
    }

    @GetMapping("/offers/details")
    public ModelAndView showOfferDetails(@RequestParam("id") Integer id, ModelMap map) {
        map.addAttribute("offer", agreementService.getOfferToConfirmById(id));
        map.addAttribute("carDto", carService.getCarDetailsByAgreementId(id));
        return new ModelAndView("agreements/offerDetails", map);
    }

    @PostMapping("/offers")
    public ModelAndView confirmOffer(@ModelAttribute("offer") @Valid NotApprovedAgreementDto offer, BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("carDto", carService.getCarDetailsByAgreementId(offer.getId()));
            return new ModelAndView("agreements/offerDetails", map);
        }

        agreementService.confirmAgreement(offer);
        return new ModelAndView("redirect:/car/list");
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteCarInfo(@PathVariable Integer id) {
        carService.deleteCar(id);
        return new ModelAndView("redirect:/car/management/list/cession");
    }

    @GetMapping("/sell/{id}")
    public ModelAndView showSellCarPage(@PathVariable Integer id, ModelMap map) {
        SellDto dto = new SellDto();
        dto.setCarId(id);
        map.addAttribute("sellDto", dto);
        return new ModelAndView("agreements/buyerForm", map);
    }

    @PostMapping("/sell")
    public ModelAndView sellCar(@ModelAttribute @Valid SellDto sellDto, BindingResult result) {

        personValidator.validateBuyerData(sellDto, result);
        if (result.hasErrors()) {
            return new ModelAndView("agreements/buyerForm");
        }

        personService.saveBuyer(sellDto);
        carService.sellCar(sellDto.getCarId(), sellDto.getPersonId());
        return new ModelAndView("redirect:/car/management/sold");
    }

    @GetMapping("/buy/{id}")
    public ModelAndView buyCar(@PathVariable Integer id, ModelMap map) {
        agreementService.isCarAvailable(id);
        carService.buyCar(id);
        map.addAttribute("offer", agreementService.getOfferToConfirmByCarId(id));
        map.addAttribute("carDto", carService.getCarDetailsById(id));
        return new ModelAndView("agreements/offerDetails", map);
    }

    @GetMapping("/add/cession")
    public ModelAndView showAddCarCessionPage(ModelMap map) {
        map.addAttribute("carAgreementDto", new CarAgreementDto());
        map.addAttribute("agreementType", 2);
        map.addAttribute("fuelTypes", carService.getAllFuelTypes());
        map.addAttribute("gearboxes", carService.getAllGearboxes());
        return new ModelAndView("car/carForm", map);
    }

    @GetMapping("/add/purchase")
    public ModelAndView showAddCarPurchasePage(ModelMap map) {
        map.addAttribute("carAgreementDto", new CarAgreementDto());
        map.addAttribute("agreementType", 1);
        map.addAttribute("fuelTypes", carService.getAllFuelTypes());
        map.addAttribute("gearboxes", carService.getAllGearboxes());
        return new ModelAndView("car/carForm", map);
    }

    @GetMapping("/update/{id}")
    public ModelAndView showUpdateCarPage(@PathVariable Integer id, ModelMap map) {
        CarAgreementDto carAgreementDto = carService.getCarForUpdateByEmployee(id);
        map.addAttribute("carAgreementDto", carAgreementDto);
        map.addAttribute("agreementType", carAgreementDto.getAgreementTypeId());
        map.addAttribute("fuelTypes", carService.getAllFuelTypes());
        map.addAttribute("gearboxes", carService.getAllGearboxes());
        return new ModelAndView("car/carForm", map);
    }

    @PostMapping("/add")
    public ModelAndView addCarAndCreateAgreement(@ModelAttribute @Valid CarAgreementDto carAgreementDto, BindingResult result, @RequestPart("file") MultipartFile file, ModelMap map) {

        carValidator.validate(carAgreementDto, result);
        if (carAgreementDto.getAgreementTypeId() == 2) {
            personValidator.validateClientData(carAgreementDto, result);
        }
        if (result.hasErrors()) {
            map.addAttribute("agreementType", carAgreementDto.getAgreementTypeId());
            map.addAttribute("fuelTypes", carService.getAllFuelTypes());
            map.addAttribute("gearboxes", carService.getAllGearboxes());
            return new ModelAndView("car/carForm", map);
        }

        if (carAgreementDto.getId() == null) {
            carService.registerNewCarWithAgreementByEmployee(carAgreementDto, file);
        } else {
            carService.updateExistingCarAndAgreementDetails(carAgreementDto, file);
        }

        return new ModelAndView("redirect:/car/list");
    }

    @GetMapping("/list/cession")
    public ModelAndView showCarInCession(ModelMap map) {
        map.addAttribute("cars", carService.getAllCarsInCession());
        map.addAttribute("title", "Lista samochodów klientów");
        return new ModelAndView("/car/carlist", map);
    }

    @GetMapping("/list/purchase")
    public ModelAndView showCarInPurchase(ModelMap map) {
        map.addAttribute("cars", carService.getAllCarsInPurchase());
        map.addAttribute("title", "Lista samochodów komisu");
        return new ModelAndView("/car/carlist", map);
    }
}
