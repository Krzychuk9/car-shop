package pl.kasprowski.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.kasprowski.dto.ContactDto;
import pl.kasprowski.entity.EmailType;
import pl.kasprowski.services.EmailService;

import javax.validation.Valid;

@Controller
public class MainController {

    @Autowired
    private EmailService emailService;

    @RequestMapping("/")
    public String redirectToMainPage() {
        return "redirect:/car/list";
    }

    @GetMapping("/contact")
    public ModelAndView showContactPage(ModelMap map) {
        map.addAttribute("contactDto", new ContactDto());
        return new ModelAndView("contact");
    }

    @PostMapping("/contact")
    public ModelAndView sendUserMessage(@ModelAttribute @Valid ContactDto contactDto, BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            return new ModelAndView("contact");
        }

        emailService.sendContactEmail(contactDto, EmailType.CONTACT);
        return new ModelAndView("redirect:/car/list");
    }
}
