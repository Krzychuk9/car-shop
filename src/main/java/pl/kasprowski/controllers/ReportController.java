package pl.kasprowski.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.kasprowski.dto.GeneratedReportDto;
import pl.kasprowski.dto.ReportDto;
import pl.kasprowski.services.ReportService;

import javax.validation.Valid;

@Controller
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @GetMapping
    public ModelAndView showGenerateReportPage(ModelMap map) {
        map.addAttribute("reportDto", new ReportDto());
        map.addAttribute("reportTypeDto", reportService.getReportTypes());
        return new ModelAndView("reports/generateReport", map);
    }

    @PostMapping
    public ModelAndView generateReports(@ModelAttribute @Valid ReportDto reportDto, BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("reportTypeDto", reportService.getReportTypes());
            return new ModelAndView("reports/generateReport", map);
        }

        GeneratedReportDto generatedReportDto = reportService.generateReport(reportDto);
        map.addAttribute("report", generatedReportDto);
        return new ModelAndView("reports/report", map);
    }
}
