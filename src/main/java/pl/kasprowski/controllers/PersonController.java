package pl.kasprowski.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.kasprowski.dto.PersonDto;
import pl.kasprowski.services.PersonService;
import pl.kasprowski.validators.PersonValidator;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class PersonController {

    @Autowired
    private PersonService personService;
    @Autowired
    private PersonValidator personValidator;

    @GetMapping("/list/clients")
    public ModelAndView showClientList(ModelMap map) {
        map.addAttribute("people", personService.findAllClients());
        return new ModelAndView("person/personList", map);
    }

    @GetMapping("/list/employee")
    public ModelAndView showEmployeeList(ModelMap map) {
        map.addAttribute("people", personService.findAllEmployee());
        return new ModelAndView("person/personList", map);
    }

    @GetMapping("/details")
    public ModelAndView showUserDetails(@RequestParam Integer id, ModelMap map) {
        map.addAttribute("personDto", personService.findById(id));
        return new ModelAndView("person/personDetails", map);
    }

    @GetMapping("/add")
    public ModelAndView showAddUserPage(ModelMap map) {
        map.addAttribute("personDto", new PersonDto());
        return new ModelAndView("person/newForm", map);
    }

    @GetMapping("/update")
    public ModelAndView updateUser(@RequestParam Integer id, ModelMap map) {
        map.addAttribute("personDto", personService.findById(id));
        return new ModelAndView("person/newForm", map);
    }

    @PostMapping
    public ModelAndView saveOrUpdateUser(@ModelAttribute @Valid PersonDto personDto, BindingResult result, ModelMap map) {

        personValidator.validate(personDto, result);
        if (result.hasErrors()) {
            return new ModelAndView("person/newForm");
        }
        personService.saveOrUpdateUser(personDto);
        return new ModelAndView("redirect:/user/list/clients");
    }

    @GetMapping("/delete")
    public ModelAndView removeUser(@RequestParam Integer id) {
        personService.removeUser(id);
        return new ModelAndView("redirect:/user/list/clients");
    }
}
