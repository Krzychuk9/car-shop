package pl.kasprowski.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.kasprowski.dto.UserDetailsDto;
import pl.kasprowski.entity.EmailType;
import pl.kasprowski.services.EmailService;
import pl.kasprowski.services.RoleService;
import pl.kasprowski.services.UserService;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private EmailService emailService;

    @GetMapping("/userList")
    public ModelAndView showUserList(ModelMap map) {
        map.addAttribute("users", userService.getAllUsers());
        return new ModelAndView("account/userList", map);
    }

    @GetMapping("/details")
    public ModelAndView showUserDetailsPage(@RequestParam("id") Integer id, ModelMap map) {
        map.addAttribute("userDetailsDto", userService.getUserDetailsDto(id));
        map.addAttribute("roles", roleService.getAllRoleDto());
        return new ModelAndView("account/userDetails", map);
    }

    @PostMapping("/details")
    public ModelAndView updateUserDetails(@ModelAttribute @Valid UserDetailsDto userDetailsDto, BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("roles", roleService.getAllRoleDto());
            return new ModelAndView("account/userDetails", map);
        }

        userService.update(userDetailsDto);
        emailService.sendAccountUpdateEmail(userDetailsDto.getEmail(), EmailType.ACCOUNT_CHANGED);
        return new ModelAndView("redirect:/admin/userList");
    }
}
