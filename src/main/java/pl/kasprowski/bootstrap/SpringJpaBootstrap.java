package pl.kasprowski.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.kasprowski.entity.*;
import pl.kasprowski.repositories.*;
import pl.kasprowski.services.RoleService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

//@Component
public class SpringJpaBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private RoleService roleService;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private AgreementTypeRepository agreementTypeRepository;
    @Autowired
    private AgreementRepository agreementRepository;
    @Autowired
    private InvoiceRepository invoiceRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FuelRepository fuelRepository;
    @Autowired
    private GearboxRepository gearboxRepository;
    @Autowired
    private BCryptPasswordEncoder encoder;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        this.addRoles();
        this.addAgreementTypes();
        this.addShopOwner();
        this.addCustomers();
        this.addEmployee();
        this.addFuelTypes();
        this.addGearboxTypes();
        this.addCars();
        this.addAgreements();
        this.addInvoice();
        this.addUser();
    }


    private void addRoles() {
        Role adminRole = new Role();
        adminRole.setName("ROLE_ADMIN");

        Role userRole = new Role();
        userRole.setName("ROLE_USER");

        Role employeeRole = new Role();
        employeeRole.setName("ROLE_EMPLOYEE");

        Role managerRole = new Role();
        managerRole.setName("ROLE_MANAGER");

        Role traderRole = new Role();
        traderRole.setName("ROLE_TRADER");

        roleService.saveRole(adminRole);
        roleService.saveRole(userRole);
        roleService.saveRole(employeeRole);
        roleService.saveRole(managerRole);
        roleService.saveRole(traderRole);
    }

    private void addAgreementTypes() {
        List<AgreementType> types = new ArrayList<>();
        AgreementType type1 = new AgreementType();
        type1.setName("PURCHASE");
        types.add(type1);

        AgreementType type2 = new AgreementType();
        type2.setName("CESSION");
        types.add(type2);

        AgreementType type3 = new AgreementType();
        type3.setName("SOLD");
        types.add(type3);

        AgreementType type4 = new AgreementType();
        type4.setName("NEW");
        types.add(type4);

        agreementTypeRepository.save(types);
    }

    private void addShopOwner() {
        Employee owner = new Employee();
        owner.setName("Salon");
        owner.setSurname("Samochodowy");

        Address address = new Address();
        address.setCity("Katowice");
        address.setCountry("Polska");
        address.setStreet("Chorzowska 6");
        owner.setAddress(address);

        owner.setNip("123-456-32-18");
        owner.setPesel("17051100000");
        owner.setHireDate(new Date());

        employeeRepository.save(owner);
    }

    private void addCustomers() {
        Customer customer = new Customer();
        customer.setName("Krzysztof");
        customer.setSurname("Kasprowski");

        Address address = new Address();
        address.setCity("Mikolow");
        address.setCountry("Polska");
        address.setStreet("Damrota 13");
        customer.setAddress(address);

        customer.setNip("123-123-123");
        customer.setPesel("92091610893");

        personRepository.save(customer);

        customer = new Customer();
        customer.setName("Dawid");
        customer.setSurname("Slowinski");

        address = new Address();
        address.setCity("Katowice");
        address.setCountry("Polska");
        address.setStreet("Katowicka 22");
        customer.setAddress(address);

        customer.setNip("321-321-321");
        customer.setPesel("88101350943");

        personRepository.save(customer);

    }

    private void addEmployee() {
        Employee employee = new Employee();
        employee.setName("Marek");
        employee.setSurname("Urzon");

        Address address = new Address();
        address.setCity("Katowice");
        address.setCountry("Polska");
        address.setStreet("Francuska 23");
        employee.setAddress(address);

        employee.setNip("987-456-123");
        employee.setPesel("85101256874");
        employee.setHireDate(new Date());

        employeeRepository.save(employee);
    }

    private void addFuelTypes() {
        List<Fuel> fuelTypes = new ArrayList<>();

        Fuel fuel = new Fuel();
        fuel.setName("Benzyna");
        fuelTypes.add(fuel);

        fuel = new Fuel();
        fuel.setName("Diesel");
        fuelTypes.add(fuel);

        fuel = new Fuel();
        fuel.setName("Benzyna+LPG");
        fuelTypes.add(fuel);

        fuel = new Fuel();
        fuel.setName("Benzyna+CNG");
        fuelTypes.add(fuel);

        fuel = new Fuel();
        fuel.setName("Elektryczny");
        fuelTypes.add(fuel);

        fuel = new Fuel();
        fuel.setName("Etanol");
        fuelTypes.add(fuel);

        fuel = new Fuel();
        fuel.setName("Hybryda");
        fuelTypes.add(fuel);

        fuel = new Fuel();
        fuel.setName("Wodór");
        fuelTypes.add(fuel);

        fuelRepository.save(fuelTypes);
    }

    private void addGearboxTypes(){
        List<Gearbox> gearboxTypes = new ArrayList<>();

        Gearbox gearbox = new Gearbox();
        gearbox.setName("Automatyczna hydrauliczna");
        gearboxTypes.add(gearbox);

        gearbox = new Gearbox();
        gearbox.setName("Automatyczna bezstopniowa");
        gearboxTypes.add(gearbox);

        gearbox = new Gearbox();
        gearbox.setName("Automatyczna dwusprzęgłowa");
        gearboxTypes.add(gearbox);

        gearbox = new Gearbox();
        gearbox.setName("Manualna");
        gearboxTypes.add(gearbox);

        gearbox = new Gearbox();
        gearbox.setName("Półautomatyczna(ASG, Tiptronic)");
        gearboxTypes.add(gearbox);

        gearboxRepository.save(gearboxTypes);
    }

    private void addCars() {
        List<Fuel> fuelTypes = fuelRepository.findAll();
        List<Gearbox> gearboxTypes = gearboxRepository.findAll();

        Car c = new Car();
        c.setDescription("Car description");
        c.setEngineCapacity(1600);
        c.setLiabilityNumber("OC10/10/2017");
        c.setMark("Renault");
        c.setModel("Megane");
        c.setTestDrives(4);
        c.setImageUrl("/img/cars/carone.jpg");
        c.setPower(150);
        c.setMileage(90000);
        c.setVin("123123123123");
        c.setYear(2014);
        c.setRegistrationNumber("SMI7R09");
        c.setFuelType(fuelTypes.get(1));
        c.setGearbox(gearboxTypes.get(4));
        c.setOwner(personRepository.findOne(2));

        carRepository.save(c);

        c = new Car();
        c.setDescription("Car description");
        c.setEngineCapacity(2000);
        c.setLiabilityNumber("OC20/30/2016");
        c.setMark("Audi");
        c.setModel("A6");
        c.setTestDrives(11);
        c.setImageUrl("/img/cars/cartwo.jpg");
        c.setPower(200);
        c.setMileage(132000);
        c.setVin("987987987");
        c.setYear(2013);
        c.setRegistrationNumber("SK99999");
        c.setFuelType(fuelTypes.get(0));
        c.setGearbox(gearboxTypes.get(2));
        c.setOwner(personRepository.findOne(1));
        c.setBought(true);

        carRepository.save(c);

        c = new Car();
        c.setDescription("Car description");
        c.setEngineCapacity(1800);
        c.setLiabilityNumber("OC12/43/2017");
        c.setMark("Toyota");
        c.setModel("Avensis");
        c.setTestDrives(2);
        c.setImageUrl("/img/cars/carthree.jpg");
        c.setPower(180);
        c.setMileage(100000);
        c.setVin("456456456");
        c.setYear(2014);
        c.setRegistrationNumber("SBI1234");
        c.setFuelType(fuelTypes.get(3));
        c.setGearbox(gearboxTypes.get(1));
        c.setOwner(personRepository.findOne(1));

        carRepository.save(c);

        c = new Car();
        c.setDescription("Car description");
        c.setEngineCapacity(1800);
        c.setLiabilityNumber("OC13/45/2017");
        c.setMark("Toyota");
        c.setModel("Avensis");
        c.setTestDrives(5);
        c.setImageUrl("/img/cars/carthree.jpg");
        c.setPower(200);
        c.setMileage(80000);
        c.setVin("123456456");
        c.setYear(2015);
        c.setRegistrationNumber("SK1234");
        c.setFuelType(fuelTypes.get(2));
        c.setGearbox(gearboxTypes.get(2));
        c.setOwner(personRepository.findOne(1));

        carRepository.save(c);

        c = new Car();
        c.setDescription("Car description");
        c.setEngineCapacity(2500);
        c.setLiabilityNumber("OC21/40/2017");
        c.setMark("Audi");
        c.setModel("A8");
        c.setTestDrives(11);
        c.setImageUrl("/img/cars/cartwo.jpg");
        c.setPower(300);
        c.setMileage(1000);
        c.setVin("987987999");
        c.setYear(2017);
        c.setRegistrationNumber("SMI666");
        c.setFuelType(fuelTypes.get(6));
        c.setGearbox(gearboxTypes.get(1));
        c.setOwner(personRepository.findOne(3));

        carRepository.save(c);
    }

    private void addAgreements() {
        Agreement a = new Agreement();
        a.setAgreementType(agreementTypeRepository.findOne(2));
        a.setAmount(new BigDecimal("90000.00"));
        a.setContent("Agreement content");
        a.setDate(new Date());
        a.setCar(carRepository.findOne(1));
        a.setPerson(personRepository.findOne(2));

        agreementRepository.save(a);

        a = new Agreement();
        a.setAgreementType(agreementTypeRepository.findOne(2));
        a.setAmount(new BigDecimal("70000.00"));
        a.setContent("Agreement content");
        a.setDate(new Date());
        a.setCar(carRepository.findOne(2));
        a.setPerson(personRepository.findOne(2));

        agreementRepository.save(a);

        a = new Agreement();
        a.setAgreementType(agreementTypeRepository.findOne(1));
        a.setAmount(new BigDecimal("14000.00"));
        a.setContent("Zakupione auto");
        a.setDate(new Date());
        a.setCar(carRepository.findOne(2));
        a.setPerson(personRepository.findOne(1));

        agreementRepository.save(a);

        a = new Agreement();
        a.setAgreementType(agreementTypeRepository.findOne(1));
        a.setAmount(new BigDecimal("120000.00"));
        a.setContent("Agreement content");
        a.setDate(new Date());
        a.setCar(carRepository.findOne(3));
        a.setPerson(personRepository.findOne(1));

        agreementRepository.save(a);

        a = new Agreement();
        a.setAgreementType(agreementTypeRepository.findOne(3));
        a.setAmount(new BigDecimal("120000.00"));
        a.setContent("Agreement content");
        a.setDate(new Date());
        a.setCar(carRepository.findOne(3));
        a.setPerson(personRepository.findOne(1));
        Car car = a.getCar();
        car.setSold(true);
        carRepository.save(car);

        agreementRepository.save(a);

        a = new Agreement();
        a.setAgreementType(agreementTypeRepository.findOne(1));
        a.setAmount(new BigDecimal("14000.00"));
        a.setContent("Agreement content");
        a.setDate(new Date());
        a.setCar(carRepository.findOne(4));
        a.setPerson(personRepository.findOne(1));

        agreementRepository.save(a);

        a = new Agreement();
        a.setAgreementType(agreementTypeRepository.findOne(4));
        a.setContent("Agreement content");
        a.setDate(new Date());
        a.setCar(carRepository.findOne(5));
        a.setPerson(personRepository.findOne(3));

        agreementRepository.save(a);
    }

    private void addUser() {
        User user = new User();

        user.setUsername("manager");
        user.setEmail("manager@gmail.com");
        user.setPassword(encoder.encode("manager"));
        user.setRoles(new HashSet<>());
        Role role = roleService.getRole("ROLE_MANAGER");
        user.getRoles().add(role);
        user.setAccountNotExpired(true);
        user.setAccountNotLocked(true);
        user.setEnabled(true);
        user.setCredentialsNotExpired(true);

        userRepository.save(user);

        Person person = personRepository.findOne(1);
        person.setUser(user);
        personRepository.save(person);

        user = new User();

        user.setUsername("krzychuk9");
        user.setEmail("kasprowski.krzysztof@gmail.com");
        user.setPassword(encoder.encode("krzychuk9"));
        user.setRoles(new HashSet<>());
        role = roleService.getRole("ROLE_USER");
        user.getRoles().add(role);
        user.setAccountNotExpired(true);
        user.setAccountNotLocked(true);
        user.setEnabled(true);
        user.setCredentialsNotExpired(true);

        userRepository.save(user);

        person = personRepository.findOne(2);
        person.setUser(user);
        personRepository.save(person);

        user = new User();

        user.setUsername("user");
        user.setEmail("user@gmail.com");
        user.setPassword(encoder.encode("user"));
        user.setRoles(new HashSet<>());
        role = roleService.getRole("ROLE_USER");
        user.getRoles().add(role);
        user.setAccountNotExpired(true);
        user.setAccountNotLocked(true);
        user.setEnabled(true);
        user.setCredentialsNotExpired(true);

        userRepository.save(user);

        person = personRepository.findOne(3);
        person.setUser(user);
        personRepository.save(person);

        user = new User();

        user.setUsername("employee");
        user.setEmail("employee@gmail.com");
        user.setPassword(encoder.encode("employee"));
        user.setRoles(new HashSet<>());
        role = roleService.getRole("ROLE_TRADER");
        user.getRoles().add(role);
        user.setAccountNotExpired(true);
        user.setAccountNotLocked(true);
        user.setEnabled(true);
        user.setCredentialsNotExpired(true);

        userRepository.save(user);

        person = personRepository.findOne(4);
        person.setUser(user);
        personRepository.save(person);
    }

    public void addInvoice() {
        AgreementType soldType = agreementTypeRepository.findOne(3);
        List<Agreement> soldAgreements = agreementRepository.findAllByAgreementType(soldType);

        soldAgreements.stream().forEach(a -> {
            Invoice invoice = new Invoice();
            Invoice savedInvoice = invoiceRepository.save(invoice);
            a.setInvoice(savedInvoice);
        });

        agreementRepository.save(soldAgreements);
    }
}
