package pl.kasprowski.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import pl.kasprowski.dto.CarAgreementDto;
import pl.kasprowski.dto.PersonDto;
import pl.kasprowski.dto.SellDto;
import pl.kasprowski.entity.Person;
import pl.kasprowski.repositories.PersonRepository;

@Component
public class PersonValidator {

    @Autowired
    private PersonRepository personRepository;

    public void validate(PersonDto personDto, Errors errors) {
        Person personFromDB = personRepository.findByPesel(personDto.getPesel());

        if (personFromDB != null) {
            if (personDto.getId() == null) {
                errors.rejectValue("pesel", "duplicate.pesel", "Użytkownik o podanym PESEL istnieje!");
            } else if (!personFromDB.getId().equals(personDto.getId())) {
                errors.rejectValue("pesel", "duplicate.pesel", "Użytkownik o podanym PESEL istnieje!");
            }
        }
    }

    public void validateBuyerData(SellDto sellDto, Errors errors) {
        Person personFromDB = personRepository.findByPesel(sellDto.getPesel());

        if (personFromDB != null) {
            if (sellDto.getName().equals(personFromDB.getName()) && sellDto.getSurname().equals(personFromDB.getSurname())) {
                sellDto.setPersonId(personFromDB.getId());
            } else {
                errors.rejectValue("pesel", "duplicate.pesel", "Sprawdź dane kupujacego!");
            }
        }
    }

    public void validateClientData(CarAgreementDto dto, Errors errors) {
        this.checkIfNotEmpty(dto.getName(), "name", errors);
        this.checkIfNotEmpty(dto.getSurname(), "surname", errors);
        this.checkIfNotEmpty(dto.getPesel(), "pesel", errors);

        Person person = personRepository.findByPesel(dto.getPesel());

        if (person != null) {
            if (!isClientDataValid(person, dto)) {
                errors.rejectValue("pesel", "cleint.data", "Sprawdź dane właściciela pojazdu!");
            }
        }
    }

    private void checkIfNotEmpty(String field, String fieldName, Errors errors) {
        if (field.equals("")) {
            errors.rejectValue(fieldName, fieldName, "Pole nie może być puste!");
        }
    }

    private boolean isClientDataValid(Person person, CarAgreementDto dto) {
        return dto.getName().equals(person.getName()) && dto.getSurname().equals(person.getSurname());
    }
}
