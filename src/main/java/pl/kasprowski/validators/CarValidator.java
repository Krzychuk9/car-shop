package pl.kasprowski.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import pl.kasprowski.dto.CarAgreementDto;
import pl.kasprowski.dto.NewCarDto;
import pl.kasprowski.repositories.CarRepository;

@Component
public class CarValidator {

    @Autowired
    private CarRepository carRepository;

    public void validate(CarAgreementDto dto, Errors errors) {
        if (dto.getId() == null && carRepository.findByVinAndSold(dto.getVin(), false) != null) {
            errors.rejectValue("vin", "duplicate.vin", "Pojazd o podanym nr VIN istnieje!");
        }
    }

    public void validate(NewCarDto dto, Errors errors) {
        if (dto.getId() == null && carRepository.findByVinAndSold(dto.getVin(), false) != null) {
            errors.rejectValue("vin", "duplicate.vin", "Pojazd o podanym nr VIN istnieje!");
        }
    }
}