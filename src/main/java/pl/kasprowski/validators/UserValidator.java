package pl.kasprowski.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import pl.kasprowski.dto.UserDto;
import pl.kasprowski.exceptions.InvalidEmailException;
import pl.kasprowski.exceptions.InvalidPasswordException;
import pl.kasprowski.exceptions.InvalidUsernameException;
import pl.kasprowski.exceptions.ValidationException;
import pl.kasprowski.repositories.UserRepository;

@Component
public class UserValidator {

    @Autowired
    private UserRepository userRepository;

    public void validate(UserDto userDto, Errors errors) {

        try {
            this.validateUsername(userDto);
            this.validateUserEmail(userDto);
            this.validateUserPassword(userDto);
        } catch (ValidationException ex) {
            errors.rejectValue(ex.getType(), "error.user", ex.getMessage());
        }
    }

    public void validatePasswordAndRejectErrors(UserDto userDto, Errors errors) {
        try {
            this.validateUserPassword(userDto);
        } catch (InvalidPasswordException ex) {
            errors.rejectValue(ex.getType(), "error.user", ex.getMessage());
        }
    }

    private void validateUsername(UserDto userDto) throws InvalidUsernameException {
        if (userRepository.findByUsername(userDto.getUsername()) != null) {
            throw new InvalidUsernameException();
        }
    }

    private void validateUserEmail(UserDto userDto) throws InvalidEmailException {
        if (userRepository.findByEmail(userDto.getEmail()) != null) {
            throw new InvalidEmailException();
        }
    }

    private void validateUserPassword(UserDto userDto) throws InvalidPasswordException {
        if (!userDto.getPassword().equals(userDto.getConfirmPassword())) {
            throw new InvalidPasswordException();
        }
    }
}
