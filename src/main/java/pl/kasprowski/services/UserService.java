package pl.kasprowski.services;


import pl.kasprowski.dto.PersonDto;
import pl.kasprowski.dto.UserDetailsDto;
import pl.kasprowski.dto.UserDto;
import pl.kasprowski.entity.User;

import java.util.List;

public interface UserService {

    List<UserDto> getAllUsers();

    UserDto getUserDtoByToken(String token);

    UserDetailsDto getUserDetailsDto(Integer id);

    PersonDto findPersonDtoByUser(User loggedInUser);

    void save(UserDto userDto);

    void update(UserDetailsDto userDetailsDto);

    void confirmAccount(String token);

    void blockAccount(User user);

    void resetPassword(UserDto userDto);

    void resetPassword(Integer id);

    void resetPassword(User user);

    void setNewPassword(UserDto userDto);
}
