package pl.kasprowski.services;

import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    String saveFileAndReturnPath(MultipartFile file);
}
