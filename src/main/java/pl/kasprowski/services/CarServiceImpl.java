package pl.kasprowski.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import pl.kasprowski.converters.*;
import pl.kasprowski.dto.*;
import pl.kasprowski.entity.*;
import pl.kasprowski.exceptions.CarNotFoundException;
import pl.kasprowski.repositories.*;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;
    @Autowired
    private FileService fileService;
    @Autowired
    private AgreementService agreementService;
    @Autowired
    private PersonService personService;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private AgreementRepository agreementRepository;
    @Autowired
    private AgreementTypeRepository agreementTypeRepository;
    @Autowired
    private FuelRepository fuelRepository;
    @Autowired
    private GearboxRepository gearboxRepository;
    @Autowired
    private AgreementToOfferDtoConverter agreementToOfferDtoConverter;
    @Autowired
    private NewCarDtoConverter newCarDtoConverter;
    @Autowired
    private CarToNewCarDtoConverter carToNewCarDtoConverter;
    @Autowired
    private CarToCarAgreementDtoConverter carToCarAgreementDtoConverter;
    @Autowired
    private CarDetailsDtoConverter carDetailsDtoConverter;
    @Autowired
    private CarAgreementDtoConverter carAgreementDtoConverter;

    @Override
    public List<CarDto> getAllCarsToSell() {
        List<Agreement> agreements = agreementRepository.findAllByAgreementTypeAndCar_SoldOrAgreementTypeAndCar_SoldAndCar_Bought(agreementTypeRepository.findOne(1), false, agreementTypeRepository.findOne(2), false, false);
        List<CarDto> cars = agreements.stream().map(a -> agreementToOfferDtoConverter.convert(a)).collect(Collectors.toList());
        return cars;
    }

    @Override
    public List<CarDto> getAllSoldCars() {
        AgreementType soldType = agreementTypeRepository.findOne(3);
        List<Agreement> soldAgreements = agreementRepository.findAllByAgreementType(soldType);
        List<CarDto> cars = soldAgreements.stream().map(a -> agreementToOfferDtoConverter.convert(a)).collect(Collectors.toList());
        return cars;
    }

    @Override
    public List<CarDto> getAllCarsInCession() {
        AgreementType cessionType = agreementTypeRepository.findOne(2);
        List<Agreement> cessionAgreements = agreementRepository.findAllByAgreementTypeAndCar_SoldAndCar_Bought(cessionType, false, false);
        List<CarDto> cars = cessionAgreements.stream().map(a -> agreementToOfferDtoConverter.convert(a)).collect(Collectors.toList());
        return cars;
    }

    @Override
    public List<CarDto> getAllCarsInPurchase() {
        AgreementType purchaseType = agreementTypeRepository.findOne(1);
        List<Agreement> purchaseAgreements = agreementRepository.findAllByAgreementTypeAndCar_Sold(purchaseType, false);
        List<CarDto> cars = purchaseAgreements.stream().map(a -> agreementToOfferDtoConverter.convert(a)).collect(Collectors.toList());
        return cars;
    }

    @Override
    public List<CarDto> getAllCarsByLoggedUser() {
        Person loggedInPerson = securityService.findLoggedInPerson();
        List<AgreementType> types = agreementTypeRepository.findAllByNameOrName("NEW", "CESSION");
        List<Agreement> cessionAgreementsByLoggedUser = agreementRepository.findAllByPersonAndAgreementTypeInAndCar_SoldAndCar_Bought(loggedInPerson, types, false, false);
        List<CarDto> cars = cessionAgreementsByLoggedUser.stream().map(a -> agreementToOfferDtoConverter.convert(a)).collect(Collectors.toList());
        return cars;
    }

    @Override
    public CarDetailsDto getCarDetailsById(Integer id) {
        Agreement agreement = agreementRepository.findFirstByCar_IdOrderByDateDesc(id);
        CarDetailsDto dto = carDetailsDtoConverter.convert(agreement);
        return dto;
    }

    @Override
    public CarDetailsDto getCarDetailsByAgreementId(Integer id) {
        Agreement agreement = agreementRepository.findOne(id);
        CarDetailsDto dto = carDetailsDtoConverter.convert(agreement);
        return dto;
    }

    @Override
    public NewCarDto getCarForUpdateByUser(Integer id) {
        Car car = this.getById(id);
        securityService.isOwner(car);
        NewCarDto carDto = carToNewCarDtoConverter.convert(car);
        return carDto;
    }

    @Override
    public CarAgreementDto getCarForUpdateByEmployee(Integer id) {
        Car car = this.getById(id);
        CarAgreementDto carDto = carToCarAgreementDtoConverter.convert(car);
        return carDto;
    }

    @Override
    public void sellCar(Integer carId, Integer personId) {
        Car car = carRepository.findOne(carId);
        Person owner = personRepository.findOne(personId);
        car.setSold(true);
        car.setOwner(owner);
        carRepository.save(car);
        agreementService.createSellAgreement(carId);
    }

    @Override
    public void buyCar(Integer id) {
        Car car = carRepository.getOne(id);
        car.setOwner(personRepository.findOne(1));
        car.setBought(true);
        carRepository.save(car);
        agreementService.createBuyAgreement(id);
    }

    @Override
    public String getVinByCarId(Integer id) {
        Car car = this.getById(id);
        return car.getVin();
    }

    @Override
    public void increaseTestDriveCount(Integer id) {
        Car car = this.getById(id);
        car.setTestDrives(car.getTestDrives() + 1);
        carRepository.save(car);
    }

    @Override
    public void deleteCar(Integer id) {
        agreementService.deleteCessionAgreementsByCarId(id);
        carRepository.delete(id);
    }

    @Override
    @Transactional
    public void registerNewCarWithAgreementByUser(NewCarDto carDto, MultipartFile file) {
        Car car = newCarDtoConverter.convert(carDto);
        car.setOwner(securityService.findLoggedInPerson());
        this.registerOrUpdateCar(car, file);
        agreementService.createNewAgreement(car);
    }

    @Override
    @Transactional
    public void registerNewCarWithAgreementByEmployee(CarAgreementDto carAgreementDto, MultipartFile file) {
        Car car = carAgreementDtoConverter.convert(carAgreementDto);
        car.setOwner(personService.getOwner(carAgreementDto));
        this.registerOrUpdateCar(car, file);
        agreementService.createAgreement(car, carAgreementDto);
    }

    @Override
    @Transactional
    public void updateExistingCarDetails(NewCarDto carDto, MultipartFile file) {
        Car car = newCarDtoConverter.convert(carDto);
        this.registerOrUpdateCar(car, file);
    }

    @Override
    @Transactional
    public void updateExistingCarAndAgreementDetails(CarAgreementDto carAgreementDto, MultipartFile file) {
        Car car = carAgreementDtoConverter.convert(carAgreementDto);
        this.registerOrUpdateCar(car, file);
        agreementService.updateAgreement(carAgreementDto);
    }

    @Override
    public List<FuelDto> getAllFuelTypes() {
        List<Fuel> fuelTypes = fuelRepository.findAll();
        List<FuelDto> dto = fuelTypes.stream().map(f -> new FuelDto(f.getId(), f.getName())).collect(Collectors.toList());
        return dto;
    }

    @Override
    public List<GearBoxDto> getAllGearboxes() {
        List<Gearbox> gearboxTypes = gearboxRepository.findAll();
        List<GearBoxDto> dto = gearboxTypes.stream().map(g -> new GearBoxDto(g.getId(), g.getName())).collect(Collectors.toList());
        return dto;
    }


    private void registerOrUpdateCar(Car car, MultipartFile file) {

        if (!file.isEmpty()) {
            String filePath = fileService.saveFileAndReturnPath(file);
            car.setImageUrl(filePath);
        }

        carRepository.save(car);
    }

    private Car getById(Integer id) {
        Car car = carRepository.findOne(id);

        if (car == null || car.isSold()) {
            throw new CarNotFoundException("Samochód nie znaleziony!");
        }

        return car;
    }
}
