package pl.kasprowski.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import pl.kasprowski.dto.ContactDto;
import pl.kasprowski.dto.UserDto;
import pl.kasprowski.entity.EmailType;
import pl.kasprowski.entity.User;
import pl.kasprowski.repositories.UserRepository;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private UserRepository userRepository;

    @Value("${spring.mail.username}")
    private String companyEmail;
    @Value("${serverUrl}")
    private String serverUrl;

    @Override
    public void sendEmail(UserDto userDto, EmailType type) {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setFrom(this.companyEmail);
        mail.setTo(userDto.getEmail());
        mail.setSubject(type.getSubject());
        mail.setText(this.serverUrl + type.getUrlType() + "?token=" + userRepository.findByEmail(userDto.getEmail()).getToken());

        javaMailSender.send(mail);
    }

    @Override
    public void sendNotificationEmail(User user, String vin, EmailType type) {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setFrom(this.companyEmail);
        mail.setTo(this.companyEmail);
        mail.setSubject(type.getSubject());

        StringBuilder sb = new StringBuilder();
        sb.append("Uzytkownik: ");
        sb.append(user.getUsername());
        sb.append(", email: ");
        sb.append(user.getEmail());
        sb.append("\nWysłał prośbę o jazdę próbną samochodem o nr VIN: ");
        sb.append(vin);
        sb.append("\nSkontatkuj się niezwłocznie!");
        mail.setText(sb.toString());

        javaMailSender.send(mail);
    }

    @Override
    public void sendContactEmail(ContactDto contactDto, EmailType type) {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(this.companyEmail);
        mail.setFrom(this.companyEmail);
        mail.setSubject(type.getSubject());

        StringBuilder sb = new StringBuilder();
        sb.append("Wiadomość z systemu komisu:\n");
        sb.append(contactDto.getMessage());
        sb.append("\nDane kontaktowe:");
        sb.append("\nEmail: ");
        sb.append(contactDto.getEmail());
        sb.append("\nTelefon: ");
        sb.append(contactDto.getPhone());
        sb.append("\nProponowana data spotkania: ");
        sb.append(contactDto.getDate());
        mail.setText(sb.toString());

        javaMailSender.send(mail);
    }

    @Override
    public void sendAccountUpdateEmail(String userEmail, EmailType type) {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(userEmail);
        mail.setFrom(this.companyEmail);
        mail.setSubject(type.getSubject());

        StringBuilder sb = new StringBuilder();
        sb.append("Wiadomość z serwisu komis samochodowy!\n");
        sb.append("Na Twoim koncie wprowadzono zmiany.\n");
        sb.append("Jeżeli nie autoryzowałeś zmian prosimy o kontakt z pracownikiem komisu.\n");
        sb.append("Dziękujemy i życzymy miłego dnia.\n");
        sb.append("Najlepszy komis na rynku!");
        mail.setText(sb.toString());

        javaMailSender.send(mail);
    }
}
