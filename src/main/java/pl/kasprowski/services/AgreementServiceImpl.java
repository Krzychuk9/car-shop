package pl.kasprowski.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kasprowski.converters.NotApprovedAgreementDtoConverter;
import pl.kasprowski.dto.CarAgreementDto;
import pl.kasprowski.dto.NotApprovedAgreementDto;
import pl.kasprowski.entity.Agreement;
import pl.kasprowski.entity.AgreementType;
import pl.kasprowski.entity.Car;
import pl.kasprowski.entity.Invoice;
import pl.kasprowski.repositories.AgreementRepository;
import pl.kasprowski.repositories.AgreementTypeRepository;
import pl.kasprowski.repositories.InvoiceRepository;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AgreementServiceImpl implements AgreementService {

    @Autowired
    private AgreementRepository agreementRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private AgreementTypeRepository agreementTypeRepository;

    @Autowired
    private NotApprovedAgreementDtoConverter notApprovedAgreementDtoConverter;

    @Override
    public void createNewAgreement(Car car) {

        Agreement agreement = new Agreement();
        agreement.setPerson(car.getOwner());
        agreement.setCar(car);
        agreement.setContent("Agreement to acceptation");
        agreement.setAgreementType(this.agreementTypeRepository.findOne(4));

        agreementRepository.save(agreement);
    }

    @Override
    public void createAgreement(Car car, CarAgreementDto carAgreementDto) {
        Agreement agreement = new Agreement();

        agreement.setAgreementType(this.agreementTypeRepository.findOne(carAgreementDto.getAgreementTypeId()));
        agreement.setCar(car);
        agreement.setAmount(carAgreementDto.getAmount());
        agreement.setContent(carAgreementDto.getContent());
        agreement.setPerson(car.getOwner());
        agreement.setDate(new Date());

        agreementRepository.save(agreement);
    }

    @Override
    public Agreement findAgreementByCarId(Integer id) {
        return agreementRepository.findFirstByCar_IdOrderByDateDesc(id);
    }

    @Override
    public List<NotApprovedAgreementDto> getOffersToConfirm() {
        List<Agreement> agreements = agreementRepository.findAllByAgreementType(this.agreementTypeRepository.findOne(4));
        List<NotApprovedAgreementDto> dtos = agreements.stream().map(a -> notApprovedAgreementDtoConverter.convert(a)).collect(Collectors.toList());
        return dtos;
    }

    @Override
    public NotApprovedAgreementDto getOfferToConfirmById(Integer agreementId) {
        Agreement agreement = agreementRepository.findOne(agreementId);
        NotApprovedAgreementDto dto = this.notApprovedAgreementDtoConverter.convert(agreement);
        return dto;
    }

    @Override
    public NotApprovedAgreementDto getOfferToConfirmByCarId(Integer carId) {
        Agreement agreement = this.findAgreementByCarId(carId);
        return this.getOfferToConfirmById(agreement.getId());
    }

    @Override
    public void confirmAgreement(NotApprovedAgreementDto offer) {
        Agreement agreement = agreementRepository.findOne(offer.getId());

        agreement.setContent(offer.getContent());
        agreement.setAmount(offer.getAmount());
        agreement.setDate(new Date());
        if (agreement.getAgreementType().getName().equals("NEW")) {
            agreement.setAgreementType(agreementTypeRepository.findOne(2));
        }

        agreementRepository.save(agreement);
    }

    @Override
    public void createSellAgreement(Integer carId) {
        AgreementType sellType = agreementTypeRepository.findOne(3);
        this.createAgreementFromExisting(carId, sellType);
        this.createInvoiceForSellAgreement(carId);
    }

    @Override
    public void createBuyAgreement(Integer carId) {
        AgreementType purchaseType = agreementTypeRepository.findOne(1);
        this.createAgreementFromExisting(carId, purchaseType);
    }

    private void createAgreementFromExisting(Integer carId, AgreementType type) {
        Agreement agreement = new Agreement();
        Agreement oldAgreement = agreementRepository.findFirstByCar_IdOrderByDateDesc(carId);

        agreement.setContent(oldAgreement.getContent());
        agreement.setAmount(oldAgreement.getAmount());
        agreement.setCar(oldAgreement.getCar());
        agreement.setPerson(oldAgreement.getCar().getOwner());
        agreement.setDate(new Date());
        agreement.setAgreementType(type);

        agreementRepository.save(agreement);
    }

    @Override
    public void deleteCessionAgreementsByCarId(Integer id) {
        Agreement agreement = agreementRepository.findFirstByCar_IdOrderByDateDesc(id);

        if (agreement != null && agreement.getAgreementType().getName().equals("CESSION")) {
            agreementRepository.delete(agreement);
        } else {
            throw new RuntimeException("Nie można usunąc tego samochodu, operacja dopuszczalna wyłącznie dla samochodów w cesji!");
        }
    }

    @Override
    public void updateAgreement(CarAgreementDto carAgreementDto) {
        Agreement agreement = agreementRepository.findFirstByCar_IdOrderByDateDesc(carAgreementDto.getId());

        agreement.setContent(carAgreementDto.getContent());
        agreement.setAmount(carAgreementDto.getAmount());

        agreementRepository.save(agreement);
    }

    @Override
    public void isCarAvailable(Integer id) {
        Agreement oldAgreement = agreementRepository.findFirstByCar_IdOrderByDateDesc(id);

        if (!oldAgreement.getAgreementType().getName().equals("CESSION")) {
            throw new RuntimeException("Nie można zakupić tego samochodu!");
        }
    }

    private void createInvoiceForSellAgreement(Integer carId) {
        Agreement sellAgreement = agreementRepository.findFirstByCar_IdOrderByDateDesc(carId);

        Invoice invoice = new Invoice();
        invoiceRepository.save(invoice);

        sellAgreement.setInvoice(invoice);
        agreementRepository.save(sellAgreement);
    }
}
