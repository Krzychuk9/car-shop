package pl.kasprowski.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import pl.kasprowski.entity.Car;
import pl.kasprowski.entity.Person;
import pl.kasprowski.entity.User;
import pl.kasprowski.exceptions.AccountNotFullyActivatedException;
import pl.kasprowski.exceptions.UserNotFoundException;
import pl.kasprowski.repositories.PersonRepository;
import pl.kasprowski.repositories.UserRepository;

@Service
public class SecurityServiceImpl implements SecurityService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PersonRepository personRepository;

    @Override
    public User findLoggedInUser() {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            UserDetails userDetails = (UserDetails) principal;
            String username = userDetails.getUsername();
            User user = userRepository.findByUsername(username);
            return user;
        } else {
            throw new UserNotFoundException("User not found");
        }
    }

    @Override
    public Person findLoggedInPerson() {
        User user = this.findLoggedInUser();
        Person person = personRepository.findByUser(user);
        return person;
    }

    @Override
    public void isAccountFullyActivated() throws AccountNotFullyActivatedException {
        Person loggedInPerson = this.findLoggedInPerson();

        if (loggedInPerson == null) {
            throw new AccountNotFullyActivatedException("Uzupełnij dane osobowe!");
        }
    }

    @Override
    public void isOwner(Car car) {
        Integer ownerId = car.getOwner().getId();
        Person person = this.findLoggedInPerson();

        if (!person.getId().equals(ownerId)) {
            throw new RuntimeException("Access denied!");
        }
    }
}
