package pl.kasprowski.services;

import pl.kasprowski.entity.Car;
import pl.kasprowski.entity.Person;
import pl.kasprowski.entity.User;
import pl.kasprowski.exceptions.AccountNotFullyActivatedException;

public interface SecurityService {
    User findLoggedInUser();

    Person findLoggedInPerson();

    void isAccountFullyActivated() throws AccountNotFullyActivatedException;

    void isOwner(Car car);
}
