package pl.kasprowski.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.kasprowski.converters.PersonDtoConverter;
import pl.kasprowski.converters.UserDtoConverter;
import pl.kasprowski.dto.PersonDto;
import pl.kasprowski.dto.UserDetailsDto;
import pl.kasprowski.dto.UserDto;
import pl.kasprowski.entity.Person;
import pl.kasprowski.entity.Role;
import pl.kasprowski.entity.User;
import pl.kasprowski.exceptions.UserNotFoundException;
import pl.kasprowski.repositories.PersonRepository;
import pl.kasprowski.repositories.RoleRepository;
import pl.kasprowski.repositories.UserRepository;
import pl.kasprowski.utils.TokenGenerator;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private UserDtoConverter userDtoConverter;
    @Autowired
    private BCryptPasswordEncoder encoder;
    @Autowired
    private TokenGenerator tokenGenerator;
    @Autowired
    private PersonDtoConverter personDtoConverter;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private PersonService personService;

    @Override
    public void save(UserDto userDto) {
        User user = userDtoConverter.convert(userDto);
        userRepository.save(user);
    }

    @Override
    public void update(UserDetailsDto userDetailsDto) {
        User user = userRepository.findOne(userDetailsDto.getId());

        user.setUsername(userDetailsDto.getUsername());
        user.setEmail(userDetailsDto.getEmail());
        user.setAccountNotLocked(userDetailsDto.isNotBlocked());

        Role role = roleRepository.findOne(userDetailsDto.getRoleId());
        user.getRoles().clear();
        user.getRoles().add(role);

        userRepository.save(user);

        if (userDetailsDto.isAccountActive() && !user.getAccountNotExpired()) {
            user.setAccountNotExpired(true);
            userRepository.save(user);
            personService.restorePerson(personRepository.findByUser(user));
        } else if (!userDetailsDto.isAccountActive() && user.getAccountNotExpired()) {
            personService.removeUser(personRepository.findByUser(user).getId());
        }
    }

    @Override
    public void confirmAccount(String token) {
        User user = userRepository.findByToken(token);

        this.checkIfUserExists(user, "Błąd aktywacji konta, skontatkuj się z pracownikiem komisu!");

        user.setEnabled(true);
        user.setToken(null);
        userRepository.save(user);
    }

    @Override
    public void resetPassword(UserDto userDto) {
        User user = userRepository.findByEmail(userDto.getEmail());

        this.checkIfUserExists(user, "Podany adres e-mail nie jest zarejestrowny!");

        this.resetPassword(user);
    }

    @Override
    public void resetPassword(Integer id) {
        User user = userRepository.findOne(id);

        this.checkIfUserExists(user, "Użytkownik o podanym id nie istnieje!");

        this.resetPassword(user);
    }

    @Override
    public void resetPassword(User user) {
        user.setPassword(null);
        user.setCredentialsNotExpired(false);
        user.setToken(tokenGenerator.getToken(user.getUsername()));
        userRepository.save(user);
    }

    @Override
    public UserDto getUserDtoByToken(String token) {
        User user = userRepository.findByToken(token);

        this.checkIfUserExists(user, "Błąd aktywacji konta, skontatkuj się z pracownikiem komisu!");

        UserDto dto = new UserDto();
        dto.setUsername(user.getUsername());
        dto.setEmail(user.getEmail());

        return dto;
    }

    @Override
    public void setNewPassword(UserDto userDto) {
        User user = userRepository.findByUsernameAndEmail(userDto.getUsername(), userDto.getEmail());

        this.checkIfUserExists(user, "Błąd zmiany hasła, skontatkuj się z pracownikiem komisu!");

        user.setPassword(encoder.encode(userDto.getPassword()));
        user.setCredentialsNotExpired(true);
        user.setToken(null);
        userRepository.save(user);
    }

    @Override
    public PersonDto findPersonDtoByUser(User loggedInUser) {
        securityService.isAccountFullyActivated();
        Person person = loggedInUser.getPerson();

        PersonDto dto = personDtoConverter.convert(person);
        return dto;
    }

    @Override
    public void blockAccount(User user) {
        user.setAccountNotExpired(false);
        userRepository.save(user);
    }

    @Override
    public List<UserDto> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserDto> usersDto = new ArrayList<>();

        users.stream().forEach(u -> {
            UserDto dto = new UserDto();
            dto.setId(u.getId());
            dto.setEmail(u.getEmail());
            dto.setUsername(u.getUsername());
            usersDto.add(dto);
        });

        return usersDto;
    }

    @Override
    public UserDetailsDto getUserDetailsDto(Integer id) {
        User user = userRepository.findOne(id);
        UserDetailsDto dto = new UserDetailsDto();

        dto.setId(user.getId());
        dto.setUsername(user.getUsername());
        dto.setEmail(user.getEmail());
        dto.setAccountActive(user.getAccountNotExpired());
        dto.setNotBlocked(user.getAccountNotLocked());
        dto.setRoleId(user.getRoles().stream().findFirst().get().getId());

        return dto;
    }

    private void checkIfUserExists(User user, String errorMessage) throws UserNotFoundException {
        if (user == null) {
            throw new UserNotFoundException(errorMessage);
        }
    }
}
