package pl.kasprowski.services;

import pl.kasprowski.dto.ContactDto;
import pl.kasprowski.dto.UserDto;
import pl.kasprowski.entity.EmailType;
import pl.kasprowski.entity.User;

public interface EmailService {
    void sendEmail(UserDto userDto, EmailType type);

    void sendNotificationEmail(User user, String vin, EmailType type);

    void sendContactEmail(ContactDto contactDto, EmailType contact);

    void sendAccountUpdateEmail(String userEmail, EmailType type);

}
