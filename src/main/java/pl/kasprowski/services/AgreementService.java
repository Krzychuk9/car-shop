package pl.kasprowski.services;

import pl.kasprowski.dto.CarAgreementDto;
import pl.kasprowski.dto.NotApprovedAgreementDto;
import pl.kasprowski.entity.Agreement;
import pl.kasprowski.entity.Car;

import java.util.List;

public interface AgreementService {

    List<NotApprovedAgreementDto> getOffersToConfirm();

    NotApprovedAgreementDto getOfferToConfirmById(Integer agreementId);

    NotApprovedAgreementDto getOfferToConfirmByCarId(Integer carId);

    Agreement findAgreementByCarId(Integer id);

    void createAgreement(Car car, CarAgreementDto carAgreementDto);

    void createNewAgreement(Car car);

    void createSellAgreement(Integer carId);

    void createBuyAgreement(Integer carId);

    void confirmAgreement(NotApprovedAgreementDto offer);

    void deleteCessionAgreementsByCarId(Integer id);

    void updateAgreement(CarAgreementDto carAgreementDto);

    void isCarAvailable(Integer id);
}
