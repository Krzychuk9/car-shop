package pl.kasprowski.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kasprowski.dto.RoleDto;
import pl.kasprowski.entity.Role;
import pl.kasprowski.repositories.RoleRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void saveRole(Role role) {
        this.roleRepository.save(role);
    }

    @Override
    public Role getRole(String roleName) {
        return roleRepository.findByName(roleName);
    }

    @Override
    public List<Role> getAll() {
        return roleRepository.findAll();
    }

    @Override
    public List<RoleDto> getAllRoleDto() {
        List<Role> roles = this.getAll();
        List<RoleDto> dto = roles.stream().map(r -> new RoleDto(r.getId(), r.getName())).collect(Collectors.toList());
        return dto;
    }
}
