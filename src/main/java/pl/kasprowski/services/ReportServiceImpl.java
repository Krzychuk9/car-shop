package pl.kasprowski.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kasprowski.converters.ReportDtoConverter;
import pl.kasprowski.dto.GeneratedReportDto;
import pl.kasprowski.dto.ReportDto;
import pl.kasprowski.dto.ReportTypeDto;
import pl.kasprowski.entity.*;
import pl.kasprowski.repositories.AgreementRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private AgreementRepository agreementRepository;
    @Autowired
    private ReportDtoConverter reportDtoConverter;

    @Override
    public List<ReportTypeDto> getReportTypes() {
        List<ReportTypeDto> reportType = new ArrayList<>();

        ReportTypeDto sellType = new ReportTypeDto(1, "Sprzedarz");
        reportType.add(sellType);

        ReportTypeDto buyType = new ReportTypeDto(2, "Zakup");
        reportType.add(buyType);

        return reportType;
    }

    @Override
    public GeneratedReportDto generateReport(ReportDto reportDto) {
        ReportData reportData = reportDtoConverter.convert(reportDto);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        GeneratedReportDto dto = new GeneratedReportDto();
        dto.setDateFrom(formatter.format(reportData.getDateFrom()));
        dto.setDateTo(formatter.format(reportData.getDateTo()));
        dto.setType(reportData.getTypId());

        List<Report> reports = this.getReports(reportData.getDateFrom(), reportData.getDateTo(), reportData.getTypId());
        dto.setAgreements(reports);

        BigDecimal sum = reports.stream().map(r -> r.getAmount()).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, RoundingMode.DOWN);
        this.calculateProfit(dto, reportData.getDateFrom(), reportData.getDateTo());
        dto.setSum(sum);

        return dto;
    }

    private List<Report> getReports(Date dateFrom, Date dateTo, Integer typId) {
        List<Report> reports = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        List<Agreement> agreements = this.getAgreementsByTypeId(dateFrom, dateTo, typId);

        Collections.sort(agreements, new Comparator<Agreement>() {
            @Override
            public int compare(Agreement o1, Agreement o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        });

        agreements.forEach(a -> {
            Car c = a.getCar();
            Person p = a.getPerson();
            Report report = new Report(a.getAmount(), a.getContent(), formatter.format(a.getDate()), c.getMark(), c.getModel(), c.getVin(), p.getName(), p.getSurname());
            reports.add(report);
        });

        return reports;
    }

    private List<Agreement> getAgreementsByTypeId(Date dateFrom, Date dateTo, Integer typId) {
        if (typId == 1) {
            return agreementRepository.findAllByDateBetweenAndAgreementType_Id(dateFrom, dateTo, 3);
        } else {
            return agreementRepository.findAllByDateBetweenAndCar_BoughtAndAgreementType_Id(dateFrom, dateTo, true, 1);
        }
    }

    private void calculateProfit(GeneratedReportDto dto, Date dateFrom, Date dateTo) {
        dto.setProfit(new BigDecimal("0.00"));
        BigDecimal profit = dto.getProfit();
        BigDecimal profitMultiplier = new BigDecimal("0.20");
        BigDecimal tax = new BigDecimal("0.19");
        List<Agreement> soldAgreements = agreementRepository.findAllByDateBetweenAndAgreementType_Id(dateFrom, dateTo, 3);

        soldAgreements.stream().forEach(a -> {
            if (a.getCar().isBought()) {
                Agreement cessionAgreement = agreementRepository.findByAgreementType_IdAndCar_IdAndCar_Bought(2, a.getCar().getId(), true);
                BigDecimal oldPrice = cessionAgreement.getAmount();
                BigDecimal profitWithTax = a.getAmount().subtract(oldPrice);
                BigDecimal profitWithoutTax = profitWithTax.subtract(profitWithTax.multiply(tax));
                dto.setProfit(dto.getProfit().add(profitWithoutTax));
            } else {
                BigDecimal profitWithoutTax = a.getAmount().multiply(profitMultiplier);
                dto.setProfit(dto.getProfit().add(profitWithoutTax));
            }
        });
        dto.setProfit(dto.getProfit().setScale(2, RoundingMode.DOWN));
    }
}
