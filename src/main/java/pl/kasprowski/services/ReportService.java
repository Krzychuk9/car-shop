package pl.kasprowski.services;

import pl.kasprowski.dto.ReportDto;
import pl.kasprowski.dto.ReportTypeDto;
import pl.kasprowski.dto.GeneratedReportDto;

import java.util.List;

public interface ReportService {
    List<ReportTypeDto> getReportTypes();

    GeneratedReportDto generateReport(ReportDto reportDto);
}
