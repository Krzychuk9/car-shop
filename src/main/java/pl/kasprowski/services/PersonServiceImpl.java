package pl.kasprowski.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kasprowski.converters.PersonConverter;
import pl.kasprowski.converters.PersonDtoConverter;
import pl.kasprowski.converters.SellDtoConverter;
import pl.kasprowski.dto.CarAgreementDto;
import pl.kasprowski.dto.PersonDto;
import pl.kasprowski.dto.SellDto;
import pl.kasprowski.entity.Customer;
import pl.kasprowski.entity.Employee;
import pl.kasprowski.entity.Person;
import pl.kasprowski.entity.User;
import pl.kasprowski.exceptions.UserNotFoundException;
import pl.kasprowski.repositories.CarRepository;
import pl.kasprowski.repositories.CustomerRepository;
import pl.kasprowski.repositories.EmployeeRepository;
import pl.kasprowski.repositories.PersonRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private PersonDtoConverter personDtoConverter;
    @Autowired
    private PersonConverter personConverter;
    @Autowired
    private SellDtoConverter sellDtoConverter;
    @Autowired
    private UserService userService;

    @Override
    public List<PersonDto> findAllClients() {
        List<Customer> people = customerRepository.findAllByRemoved(false);
        List<PersonDto> dto = people.stream().map(p -> personDtoConverter.convert(p)).collect(Collectors.toList());
        return dto;
    }

    @Override
    public List<PersonDto> findAllEmployee() {
        List<Employee> people = employeeRepository.findAllByRemoved(false);
        List<PersonDto> dto = people.stream().map(p -> personDtoConverter.convert(p)).collect(Collectors.toList());
        return dto;
    }

    @Override
    public PersonDto findById(Integer id) {
        Person person = personRepository.findOne(id);
        if (person == null || person.isRemoved()) {
            throw new UserNotFoundException("Użytkownik nie znalezniony!");
        }
        PersonDto dto = personDtoConverter.convert(person);
        return dto;
    }

    @Override
    public void removeUser(Integer id) {
        if (this.hasAnyCarsInCession(id)) {
            throw new RuntimeException("Nie można usunąć klienta, który posiada samochody w sprzedaży!");
        }
        Person person = personRepository.findOne(id);
        person.setRemoved(true);
        User user = person.getUser();
        if (user != null) {
            userService.blockAccount(user);
        }
        personRepository.save(person);
    }

    @Override
    public void saveOrUpdateUser(PersonDto personDto) {
        Person person = personConverter.convert(personDto);
        personRepository.save(person);
    }

    @Override
    public void saveBuyer(SellDto sellDto) {
        if (sellDto.getPersonId() == null) {
            Person person = sellDtoConverter.convert(sellDto);
            Person personDB = personRepository.save(person);
            sellDto.setPersonId(personDB.getId());
        }
    }

    @Override
    public Person getOwner(CarAgreementDto dto) {
        if (dto.getAgreementTypeId() == 1) {
            return this.getOwnerForPurchase();
        } else {
            return this.getOwnerForCession(dto);
        }
    }

    @Override
    public void restorePerson(Person person) {
        person.setRemoved(false);
        personRepository.save(person);
    }

    private boolean hasAnyCarsInCession(Integer personId) {
        return !carRepository.findAllByOwner_Id(personId).isEmpty();
    }

    private Person getOwnerForPurchase() {
        return personRepository.findOne(1);
    }

    private Person getOwnerForCession(CarAgreementDto dto) {
        Person owner = personRepository.findByPesel(dto.getPesel());

        if (owner == null) {
            owner = new Customer();
            owner.setPesel(dto.getPesel());
            owner.setName(dto.getName());
            owner.setSurname(dto.getSurname());
            owner = personRepository.save(owner);
        }

        return owner;
    }
}
