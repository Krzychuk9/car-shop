package pl.kasprowski.services;

import org.springframework.web.multipart.MultipartFile;
import pl.kasprowski.dto.*;

import java.util.List;

public interface CarService {

    List<CarDto> getAllCarsToSell();

    List<CarDto> getAllSoldCars();

    List<CarDto> getAllCarsInCession();

    List<CarDto> getAllCarsInPurchase();

    List<CarDto> getAllCarsByLoggedUser();

    CarDetailsDto getCarDetailsById(Integer id);

    CarDetailsDto getCarDetailsByAgreementId(Integer id);

    NewCarDto getCarForUpdateByUser(Integer id);

    CarAgreementDto getCarForUpdateByEmployee(Integer id);

    void sellCar(Integer carId, Integer buyerId);

    void deleteCar(Integer id);

    void registerNewCarWithAgreementByUser(NewCarDto newCarDto, MultipartFile file);

    void registerNewCarWithAgreementByEmployee(CarAgreementDto carDto, MultipartFile file);

    void updateExistingCarDetails(NewCarDto newCarDto, MultipartFile file);

    void updateExistingCarAndAgreementDetails(CarAgreementDto carAgreementDto, MultipartFile file);

    void buyCar(Integer id);

    String getVinByCarId(Integer id);

    void increaseTestDriveCount(Integer id);

    List<FuelDto> getAllFuelTypes();

    List<GearBoxDto> getAllGearboxes();
}
