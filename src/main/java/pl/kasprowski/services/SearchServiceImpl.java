package pl.kasprowski.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kasprowski.dto.CarDto;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private CarService carService;

    @Override
    public List<CarDto> getAllCarsToSell(String searchDto) {
        List<CarDto> allCarsToSell = carService.getAllCarsToSell();
        String searchText = searchDto.toLowerCase();

        List<CarDto> cars = allCarsToSell.stream().filter(c -> {
            String carDetails = (c.getModel() + " " + c.getMark() + " " + c.getModel()).toLowerCase();
            return carDetails.contains(searchText);
        }).collect(Collectors.toList());

        return cars;
    }
}
