package pl.kasprowski.services;

import pl.kasprowski.dto.CarAgreementDto;
import pl.kasprowski.dto.PersonDto;
import pl.kasprowski.dto.SellDto;
import pl.kasprowski.entity.Person;

import java.util.List;

public interface PersonService {
    List<PersonDto> findAllClients();

    List<PersonDto> findAllEmployee();

    PersonDto findById(Integer id);

    void removeUser(Integer id);

    void saveOrUpdateUser(PersonDto personDto);

    void saveBuyer(SellDto sellDto);

    Person getOwner(CarAgreementDto carAgreementDto);

    void restorePerson(Person person);
}
