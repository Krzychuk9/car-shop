package pl.kasprowski.services;

import pl.kasprowski.dto.RoleDto;
import pl.kasprowski.entity.Role;

import java.util.List;

public interface RoleService {

    void saveRole(Role role);

    Role getRole(String roleName);

    List<Role> getAll();

    List<RoleDto> getAllRoleDto();
}
