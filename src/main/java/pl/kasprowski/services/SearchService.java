package pl.kasprowski.services;

import pl.kasprowski.dto.CarDto;

import java.util.List;

public interface SearchService {

    List<CarDto> getAllCarsToSell(String searchDto);
}
