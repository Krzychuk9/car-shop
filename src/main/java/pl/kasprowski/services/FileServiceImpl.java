package pl.kasprowski.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class FileServiceImpl implements FileService {

    @Value("${img.file.path}")
    private String PATH;

    @Override
    public String saveFileAndReturnPath(MultipartFile file) {

        try {
            byte[] bytes = file.getBytes();
            FileOutputStream out = new FileOutputStream(PATH + file.getOriginalFilename());
            out.write(bytes);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("File not uploaded!");
        }

        return "/img/cars/" + file.getOriginalFilename();
    }
}
