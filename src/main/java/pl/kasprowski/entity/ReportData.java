package pl.kasprowski.entity;

import java.util.Date;

public class ReportData {

    private Integer typId;
    private Date dateTo;
    private Date dateFrom;

    public ReportData() {
    }

    public ReportData(Integer typId, Date dateTo, Date dateFrom) {
        this.typId = typId;
        this.dateTo = dateTo;
        this.dateFrom = dateFrom;
    }

    public Integer getTypId() {
        return typId;
    }

    public void setTypId(Integer typId) {
        this.typId = typId;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }
}
