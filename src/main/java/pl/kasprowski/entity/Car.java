package pl.kasprowski.entity;

import javax.persistence.*;

@Entity
public class Car extends AbstractEntity {
    @Column
    private String vin;
    @Column
    private Integer year;
    @Column
    private String mark;
    @Column
    private String model;
    @Column
    private String liabilityNumber;
    @Column
    private String registrationNumber;
    @ManyToOne
    @JoinColumn(name = "fuel_type")
    private Fuel fuelType;
    @Column
    private Integer mileage;
    @Column
    private Integer engineCapacity;
    @Column
    private Integer power;
    @ManyToOne
    @JoinColumn(name = "gearbox_type")
    private Gearbox gearbox;
    @Column
    private String description;
    @Column
    private Integer testDrives;
    @Column
    private String imageUrl;
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Person owner;
    @Column
    private boolean sold;
    @Column
    private boolean bought;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLiabilityNumber() {
        return liabilityNumber;
    }

    public void setLiabilityNumber(String liabilityNumber) {
        this.liabilityNumber = liabilityNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Fuel getFuelType() {
        return fuelType;
    }

    public void setFuelType(Fuel fuelType) {
        this.fuelType = fuelType;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Integer getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(Integer engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Gearbox getGearbox() {
        return gearbox;
    }

    public void setGearbox(Gearbox gearbox) {
        this.gearbox = gearbox;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTestDrives() {
        return testDrives;
    }

    public void setTestDrives(Integer testDrives) {
        this.testDrives = testDrives;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public boolean isSold() {
        return sold;
    }

    public void setSold(boolean sold) {
        this.sold = sold;
    }

    public boolean isBought() {
        return bought;
    }

    public void setBought(boolean bought) {
        this.bought = bought;
    }
}
