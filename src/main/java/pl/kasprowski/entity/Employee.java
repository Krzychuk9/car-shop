package pl.kasprowski.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@DiscriminatorValue(value = "employee")
public class Employee extends Person {

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date hireDate;

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }
}
