package pl.kasprowski.entity;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    @Column(name = "added", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date dateAdded;

    @Column(name = "updated", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date dateUpdated;

    @PrePersist
    @PreUpdate
    public void updateTimeStamps() {
        dateUpdated = new Date();
        if (dateAdded == null) {
            dateAdded = new Date();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }
}
