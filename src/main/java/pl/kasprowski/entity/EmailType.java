package pl.kasprowski.entity;

public enum EmailType {
    ACCOUNT_CONFIRMATION("confirmation", "Aktywacja konta"), PASSWORD_RESET("password", "Reset hasła"), NOTIFICATION("notification","powiadomienie"), CONTACT("contact","Kontakt w sprawie spotkania"), ACCOUNT_CHANGED("accountChanged","Zmiany na Twoim koncie w serwisie auto komis!");

    EmailType(String urlType, String subject) {
        this.urlType = urlType;
        this.subject = subject;
    }

    private String subject;
    private String urlType;

    public String getSubject() {
        return subject;
    }

    public String getUrlType() {
        return urlType;
    }
}
