package pl.kasprowski.entity;

import java.math.BigDecimal;

public class Report {

    private BigDecimal amount;
    private String content;
    private String date;

    private String mark;
    private String model;
    private String vin;

    private String buyerName;
    private String buyerSurname;

    public Report() {
    }

    public Report(BigDecimal amount, String content, String date, String mark, String model, String vin, String buyerName, String buyerSurname) {
        this.amount = amount;
        this.content = content;
        this.date = date;
        this.mark = mark;
        this.model = model;
        this.vin = vin;
        this.buyerName = buyerName;
        this.buyerSurname = buyerSurname;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerSurname() {
        return buyerSurname;
    }

    public void setBuyerSurname(String buyerSurname) {
        this.buyerSurname = buyerSurname;
    }

}
