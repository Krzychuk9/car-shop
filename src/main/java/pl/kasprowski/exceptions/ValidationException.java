package pl.kasprowski.exceptions;


public class ValidationException extends RuntimeException {

    private String type;

    public ValidationException(String message, String type) {
        super(message);
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
