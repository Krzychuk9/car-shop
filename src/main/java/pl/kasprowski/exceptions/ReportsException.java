package pl.kasprowski.exceptions;

public class ReportsException extends RuntimeException {

    public ReportsException(String message) {
        super(message);
    }
}
