package pl.kasprowski.exceptions;


public class InvalidUsernameException extends ValidationException {

    public InvalidUsernameException() {
        super("Użytkownik o podanej nazwie istnieje!", "username");
    }
}
