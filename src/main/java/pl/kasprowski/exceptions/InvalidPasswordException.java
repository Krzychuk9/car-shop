package pl.kasprowski.exceptions;

public class InvalidPasswordException extends ValidationException {

    public InvalidPasswordException() {
        super("Hasło niepoprawne!", "password");
    }
}
