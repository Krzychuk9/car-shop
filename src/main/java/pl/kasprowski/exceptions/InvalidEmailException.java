package pl.kasprowski.exceptions;

public class InvalidEmailException extends ValidationException {

    public InvalidEmailException() {
        super("Użytkownik o podanym adresie e-mail istnieje!", "email");
    }
}
