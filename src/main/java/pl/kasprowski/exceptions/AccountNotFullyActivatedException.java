package pl.kasprowski.exceptions;

public class AccountNotFullyActivatedException extends RuntimeException {

    public AccountNotFullyActivatedException(String message) {
        super(message);
    }
}
