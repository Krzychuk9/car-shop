package pl.kasprowski.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.entity.Person;
import pl.kasprowski.entity.User;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {

    Person findByPesel(String pesel);

    Person findByUser(User user);
}
