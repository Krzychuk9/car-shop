package pl.kasprowski.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.entity.Fuel;

@Repository
public interface FuelRepository extends JpaRepository<Fuel, Integer> {
}
