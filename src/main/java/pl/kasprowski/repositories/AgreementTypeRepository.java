package pl.kasprowski.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.entity.AgreementType;

import java.util.List;

@Repository
public interface AgreementTypeRepository extends JpaRepository<AgreementType, Integer> {
    List<AgreementType> findAllByNameOrName(String purchase, String cession);
}
