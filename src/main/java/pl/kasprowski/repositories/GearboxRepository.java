package pl.kasprowski.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.entity.Gearbox;

@Repository
public interface GearboxRepository extends JpaRepository<Gearbox, Integer>{
}
