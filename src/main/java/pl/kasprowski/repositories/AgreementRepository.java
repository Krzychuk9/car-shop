package pl.kasprowski.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.entity.Agreement;
import pl.kasprowski.entity.AgreementType;
import pl.kasprowski.entity.Person;

import java.util.Date;
import java.util.List;

@Repository
public interface AgreementRepository extends JpaRepository<Agreement, Integer> {

    List<Agreement> findAllByAgreementTypeAndCar_SoldOrAgreementTypeAndCar_SoldAndCar_Bought(AgreementType purchaseType, boolean isSold, AgreementType cessionType, boolean sold, boolean bought);

    List<Agreement> findAllByAgreementType(AgreementType agreementType);

    List<Agreement> findAllByAgreementTypeAndCar_Sold(AgreementType agreementType, boolean isSold);

    List<Agreement> findAllByAgreementTypeAndCar_SoldAndCar_Bought(AgreementType type, boolean isSold, boolean isBought);

    List<Agreement> findAllByPersonAndAgreementTypeInAndCar_SoldAndCar_Bought(Person owner, List<AgreementType> type, boolean isSold, boolean isBought);

    List<Agreement> findAllByDateBetweenAndCar_BoughtAndAgreementType_Id(Date dateFrom, Date DateTo, boolean bought, Integer id);

    List<Agreement> findAllByDateBetweenAndAgreementType_Id(Date dateFrom, Date DateTo, Integer id);

    Agreement findFirstByCar_IdOrderByDateDesc(Integer id);

    Agreement findByAgreementType_IdAndCar_IdAndCar_Bought(Integer id, Integer carId ,boolean bought);
}
